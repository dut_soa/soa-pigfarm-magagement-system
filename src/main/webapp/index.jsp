<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
	<jsp:include page="/WEB-INF/common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/common/_navbar.jsp"></jsp:include>
	<!-- 
	<nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Trang trại nuôi heo thịt</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse pull-right">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Trang chủ</a></li>
            <li><a href="<%=request.getContextPath() + "/login"%>">Đăng nhập</a></li>
            <li><a href="contact.html">Liên hệ</a></li>
          </ul>
        </div>
      </div>
    </nav>
	 -->
	 
    <div class="container">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="images/trang_trai_2.png" width="100%" height="500" alt="Trang trai 1">
            <div class="carousel-caption">
            Trang Trai 1
            </div>
          </div>
          <div class="item">
            <img src="images/trang_trai_2.png" width="100%" height="500" alt="Trang trai 2">
            <div class="carousel-caption">
              Trang Trai 2
            </div>
          </div>

           <div class="item">
            <img src="images/trang_trai_3.png" width="100%" height="500" alt="Trang trai 2">
            <div class="carousel-caption">
              Trang Trai 3
            </div>
          </div>
          
        </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    </div>

</body>
</html>
