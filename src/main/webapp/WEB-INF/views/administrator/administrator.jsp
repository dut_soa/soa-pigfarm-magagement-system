<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
	<jsp:include page="/WEB-INF/common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<table class="table table-bordered tabler-hovered">
		<caption>System Overview</caption>
			<tr>
				<th>Models</th>
				<th>Actions</th>
				<th>Routes (JSP Views)</th>
				<th>APIs</th>
			</tr>
			
			<!-- ACCOUNT -->
			<tr>
				<td rowspan="7">Account</td>
				<td>Index (List)</td>
				<td><a href="account">/account</a> (GET)</td>
				<td><a href="api/account/all">/api/account/all</a> (GET)</td>
			</tr>
			<tr>
				<td>Create (Insert / Add)</td>
				<td><a href="account/create">/account/create</a> (GET)</td>
				<td><a href="api/account/create">/api/account/create</a> (POST)</td>
			</tr>
			<tr>
				<td>Read (View)</td>
				<td><a href="account/view?id=1">/account/view?id={ID}</a> (GET)</td>
				<td><a href="api/account/view?id=1">/api/account/view?id={ID}</a> (GET)</td>
			</tr>
			<tr>
				<td>Update (Edit)</td>
				<td><a href="account/update?id=1">/account/update?id={ID}</a> (GET)</td>
				<td><a href="api/account/update?id=1">/api/account/update</a> (POST)</td>
			</tr>
			<tr>
				<td>Delete (Remove)</td>
				<td>-</td>
				<td><a href="api/account/delete?id=1">/api/account/delete</a> (GET)</td>
			</tr>
			<tr>
				<td>Change Password</td>
				<td><a href="account/change-password?id=1">/account/change-password?id={ID}</a> (GET)</td>
				<td><a href="#">/api/account/change-password</a> (POST)</td>
			</tr>
			<tr>
				<td>Login</td>
				<td><a href="login">/login</a> (GET)</td>
				<td><a href="api/account/login">/api/account/login</a> (POST)</td>
			</tr>
			
			<!-- block -->
			<tr>
				<td rowspan="5">Block</td>
				<td>Index (List)</td>
				<td><a href="block">/block</a> (GET)</td>
				<td><a href="api/block/all">/api/block/all</a> (GET)</td>
			</tr>
			<tr>
				<td>Create (Insert / Add)</td>
				<td><a href="block/create">/block/create</a> (GET)</td>
				<td><a href="api/block/create">/api/block/create</a> (POST)</td>
			</tr>
			<tr>
				<td>Read (View)</td>
				<td><a href="block/view?id=1">/block/view?id={ID}</a> (GET)</td>
				<td><a href="api/block/view?id=1">/api/block/view?id={ID}</a> (GET)</td>
			</tr>
			<tr>
				<td>Update (Edit)</td>
				<td><a href="block/update?id=1">/block/update?id={ID}</a> (GET)</td>
				<td><a href="api/block/update?id=1">/api/block/update</a> (POST)</td>
			</tr>
			<tr>
				<td>Delete (Remove)</td>
				<td>-</td>
				<td><a href="api/block/delete?id=1">/api/block/delete</a> (GET)</td>
			</tr>
			
			<!-- Pigpen -->
			<tr>
				<td rowspan="6">Pigpen</td>
				<td>Index (List)</td>
				<td><a href="pigpen">/pigpen</a> (GET)</td>
				<td><a href="api/pigpen/all">/api/pigpen/all</a> (GET)</td>
			</tr>
			<tr>
				<td>Index (List in a Block)</td>
				<td><a href="pigpen?blockId=1">/pigpen?blockId=1</a> (GET)</td>
				<td><a href="api/pigpen/all?blockId=1">/api/pigpen/all?blockId=1</a> (GET)</td>
			</tr>
			<tr>
				<td>Create (Insert / Add)</td>
				<td><a href="pigpen/create">/pigpen/create</a> (GET)</td>
				<td><a href="api/pigpen/create">/api/pigpen/create</a> (POST)</td>
			</tr>
			<tr>
				<td>Read (View)</td>
				<td><a href="pigpen/view?id=1">/pigpen/view?id={ID}</a> (GET)</td>
				<td><a href="api/pigpen/view?id=1">/api/pigpen/view?id={ID}</a> (GET)</td>
			</tr>
			<tr>
				<td>Update (Edit)</td>
				<td><a href="pigpen/update?id=1">/pigpen/update?id={ID}</a> (GET)</td>
				<td><a href="api/pigpen/update?id=1">/api/pigpen/update</a> (POST)</td>
			</tr>
			<tr>
				<td>Delete (Remove)</td>
				<td>-</td>
				<td><a href="api/pigpen/delete?id=1">/api/pigpen/delete</a> (GET)</td>
			</tr>
			
			<!-- Race -->
			<tr>
				<td rowspan="5">Race</td>
				<td>Index (List)</td>
				<td><a href="race">/race</a> (GET)</td>
				<td><a href="api/race/all">/api/race/all</a> (GET)</td>
			</tr>
			<tr>
				<td>Create (Insert / Add)</td>
				<td><a href="race/create">/race/create</a> (GET)</td>
				<td><a href="api/race/create">/api/race/create</a> (POST)</td>
			</tr>
			<tr>
				<td>Read (View)</td>
				<td><a href="race/view?id=1">/race/view?id={ID}</a> (GET)</td>
				<td><a href="api/race/view?id=1">/api/race/view?id={ID}</a> (GET)</td>
			</tr>
			<tr>
				<td>Update (Edit)</td>
				<td><a href="race/update?id=1">/race/update?id={ID}</a> (GET)</td>
				<td><a href="api/race/update">/api/race/update</a> (POST)</td>
			</tr>
			<tr>
				<td>Delete (Remove)</td>
				<td>-</td>
				<td><a href="api/race/delete?id=1">/api/race/delete</a> (GET)</td>
			</tr>
			
			<!-- Pig Price -->
			<tr>
				<td rowspan="3">Pig Price</td>
				<td>Bảng giá hiện tại</td>
				<td><a href="pig-price/show">/pig-price/show</a> (GET)</td>
				<td><a href="api/pig-price/show">/api/pig-price/show</a> (GET)</td>
			</tr>
			<tr>
				<td>Lịch sử bảng giá</td>
				<td><a href="pig-price">pig-price</a> (GET)</td>
				<td><a href="api/pig-price/all">/api/pig-price/all</a> (GET)</td>
			</tr>
			<tr>
				<td>Cập nhật giá mới</td>
				<td><a href="pig-price/update?id=1">/pig-price/update?id={ID}</a> (GET)</td>
				<td><a href="api/pig-price/update">/api/pig-price/update</a> (POST)</td>
			</tr>
			
			<!-- Health Journal -->
			<tr>
				<td rowspan="2">Health Journal</td>
				<td>Bảng theo dõi sức khỏe</td>
				<td><a href="health-journal">/health-journal</a> (GET)</td>
				<td><a href="api/health-journal/all">api/health-journal/all</a> (GET)</td>
			</tr>
			<tr>
				<td>Trình trạng sức khỏe heo</td>
				<td><a href="health-journal/view">/health-journal/view</a> (GET)</td>
				<td><a href="api/health-journal/view">/api/health-journal/view</a> (GET)</td>
			</tr>
		</table>
	</div>
	
	<script type="text/javascript">
	$().ready(function() {
		$(".navbar").removeClass("navbar-default");
		$(".navbar").addClass("navbar-inverse");
	});
	</script>
	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>
