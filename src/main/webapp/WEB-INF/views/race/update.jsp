<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update Race</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<h1>Cập nhật thông tin chủng loại</h1>
		<jsp:include page="_form.jsp"></jsp:include>
	</div>
	
	<script type="text/javascript">
	<% 
	int id = 0;
	try {
		id = Integer.parseInt(request.getAttribute("id").toString());
	} catch (NumberFormatException e) {} catch(NullPointerException e) {}
	%>
	$().ready(function() {
		$("#RaceForm").attr("action", "<%=request.getContextPath() + "/api/race/update"%>");
		$("#id").val(<%=id%>);
		$("#submit").html("Cập nhật");
		
		$.ajax({url: '<%=request.getContextPath() + "/api/race/view?id=" + id%>',
			success: function(json) {
				if (json.error) {
					
				} else {
					$("#name").val(json.name);
					$("#description").val(json.description);
					$("#total_growing_days").val(json.totalGrowingDays);
				}
	    	}
		}); // .ajax()
		
	});
	</script>	

	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>