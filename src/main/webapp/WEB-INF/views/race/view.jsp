<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Xem chủng loại</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<h1 id="heading"></h1>
		<table class="table table-hover" id="RaceDetailTable">
			<caption>Thông tin chi tiết</caption>
			<tr>
				<td>ID</td>
				<td id="id"></td>
			</tr>
			<tr>
				<td>Tên chủng loại</td>
				<td id="name"></td>
			</tr>
			<tr>
				<td>Mô tả</td>
				<td id="description"></td>
			</tr>
			<tr>
				<td>Số ngày tăng trưởng</td>
				<td id="total_growing_days"></td>
			</tr>
		</table>
		<div class="row">
			<div class="col-sm-2">
				<a href="<%=request.getContextPath() + "/race" %>" class="btn btn-default btn-block">
					 <i class="glyphicon glyphicon-triangle-left"></i> Index </a>
			</div>
			<div class="col-sm-2">
				<a href="" id="updateLink" class="btn btn-block btn-success">
					<i class="glyphicon glyphicon-pencil"></i> Cập nhật
				</a>
			</div>
			<div class="col-sm-2">
				<a href="#" id="deleteLink" class="btn btn-block btn-danger" onClick="return confirmDelete()">
					<i class="glyphicon glyphicon-trash"></i> Xóa
				</a>
			</div>
		</div>
		<script>
			<%
			int id = Integer.parseInt(request.getAttribute("id").toString());
			%>
			$().ready(function() {
				$.ajax({url: '<%=request.getContextPath()%>/api/race/view?id=<%=id%>', 
					success: function(json) {
						if (!json.error) {
							$('#heading').html('Race ' + json.id + ': ' + json.name);
							$('#id').html(json.id);
							$('#name').html(json.name);
							$('#description').html(json.description);
							$('#total_growing_days').html(json.totalGrowingDays);
							$('#updateLink').attr('href', '<%=request.getContextPath() + "/race/update?id=" %>' + json.id);
							$('#deleteLink').attr('href', '<%=request.getContextPath() + "/api/race/delete?id=" %>' + json.id);
						} else {
							alert ('Lỗi !');
						}
			    	}
				});
			});
			function confirmDelete() {
				return confirm('Chắc chắn xoá?');
			}
		</script>
	</div>
	
	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>