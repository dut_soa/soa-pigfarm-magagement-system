<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<form class="form form-horizontal" id="RaceForm" name="RaceForm" method="post"
	action="<%=request.getContextPath() + "/api/race/create"%>">
	
	<input id="id" type="hidden" name="id">
	
	<div class="form-group">
		<label class="col-sm-3 control-label">Tên chủng loại <span
			class="text text-danger">*</span></label>
		<div class="col-sm-9">
			<input type="text" name="name" id="name" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">Mô tả <span
			class="text text-danger">*</span></label>
		<div class="col-sm-9">
			<input type="text" name="description" id="description" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">Số ngày tăng trưởng <span
			class="text text-danger">*</span></label>
		<div class="col-sm-9">
			<input type="text" name="total_growing_days" id="total_growing_days" class="form-control">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-2">
			<a href="<%=request.getContextPath() + "/race"%>" class="btn btn-default btn-block">
				 <i class="glyphicon glyphicon-triangle-left"></i> Index </a>
		</div>
		<div class="col-sm-offset-4 col-sm-2">
			<button type="reset" class="btn btn-warning btn-block">Reset</button>
		</div>
		<div class="col-sm-2">
			<button id="submit" type="submit" class="btn btn-success btn-block">Tạo mới</button>
		</div>
	</div>
</form>

<jsp:include page="../../common/_jquery_validator.jsp"></jsp:include>
<script type="text/javascript">
	$().ready(function() {
		$('#RaceForm').validate({
			rules : {
				name : {
					required : true,
					maxlength : 50
				},
				description: {
					required: true,
					maxlength: 500
				},
				total_growing_days: {
					required: true,
					number: true
				}
			},
			errorElement: "em",
	        errorPlacement: function (error, element) {
	            // Add the `help-block` class to the error element
	            error.addClass("help-block");
				error.insertAfter(element);
	        },
	        highlight: function (element, errorClass, validClass) {
	            $(element).closest(".form-group").addClass("has-error").removeClass("has-success");
	        },
	        unhighlight: function (element, errorClass, validClass) {
	            $(element).closest(".form-group").removeClass("has-error");
	        }
		}); // .validate()
		
	});
</script>