<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update Block</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<h1>Cập nhật thông tin tài khoản</h1>
		<jsp:include page="_form.jsp"></jsp:include>
	</div>

	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>