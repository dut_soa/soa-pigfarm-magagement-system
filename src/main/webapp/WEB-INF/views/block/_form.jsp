<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	/* Common form for Create and Update actions */
	boolean isNewRecord = true;
	int id = 0;
	try {
		 id = Integer.parseInt(request.getAttribute("id").toString());
		isNewRecord = (id <= 0) ? true : false;
	} catch (NumberFormatException e) {} catch(NullPointerException e) {}
	String hiddenId = isNewRecord ? ("") : ("<input type=\"hidden\" name=\"id\" value=\"" + id +"\">");
	String formAction = isNewRecord ? request.getContextPath() + "/api/block/create" : request.getContextPath() + "/api/block/update";
	String formMethod = isNewRecord ? "post" : "post"; // 2nd: PUT
	String submitButtonLabel = isNewRecord ? "Tạo mới" : "Cập nhật";
%>
<form class="form form-horizontal" id="BlockForm" name="AccountForm" method="<%=formMethod %>" action="<%=formAction %>">
	
	<%=hiddenId %>
	
	<div class="form-group">
		<label class="col-md-3 control-label">Tên <span
			class="text text-danger">*</span></label>
		<div class="col-md-9">
			<input type="text" name="name" id="name"class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 control-label">Người quản lí</label>
		<div class="col-md-9">
			<select name="managerId" id="managerId" class="form-control">
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<a href="<%=request.getContextPath() + "/block"%>" class="btn btn-default btn-block">
				 <i class="glyphicon glyphicon-triangle-left"></i> Index </a>
		</div>
		<div class="col-md-offset-4 col-md-2">
			<button type="reset" class="btn btn-warning btn-block">Reset</button>
		</div>
		<div class="col-md-2">
			<button type="submit" class="btn btn-success btn-block"><%=submitButtonLabel %></button>
		</div>
	</div>
</form>

<jsp:include page="../../common/_jquery_validator.jsp"></jsp:include>
<script type="text/javascript">
	$().ready(function() {
		$('#BlockForm').validate({
			rules : {
				name : {
					required : true,
					maxlength : 50
				}
			},
	        errorElement: "em",
	        errorPlacement: function (error, element) {
	            // Add the `help-block` class to the error element
	            error.addClass("help-block");
				error.insertAfter(element);
	        },
	        highlight: function (element, errorClass, validClass) {
	            $(element).closest(".form-group").addClass("has-error").removeClass("has-success");
	        },
	        unhighlight: function (element, errorClass, validClass) {
	            $(element).closest(".form-group").removeClass("has-error");
	        }
		}); // .validate()
		
		$.ajax({url: '<%=request.getContextPath() + "/api/account/all"%>', 
			success: function(json) {
				if (json.error) {
					
				} else {
					for (i = 0; i < json.length; i++) {
						var item = '<option value="' + json[i].id + '">' + json[i].fullName + '</option>';
						$('#managerId').append(item);
					}
					
					$.ajax({
						url: '<%=request.getContextPath() + "/api/block/view?id=" + id%>', 
						success: function(json2) {
							if (json2.error) {
								
							} else {
								$('#name').val(json2.name);
								$('#managerId option[value=' + json2.manager.id + ']').prop('selected', true);
							}
				    	}
					}); // .ajax()
				}
	    	}
		}); // .ajax()
		
		
		
		
	});
</script>