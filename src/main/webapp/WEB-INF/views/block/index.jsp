<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách các khu vực trang trại</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<h1>Block</h1>
		<div class="row">
			<div class="col-md-2">
				<a href="<%=request.getContextPath() + "/block/create" %>" class="btn btn-block btn-success">Tạo mới</a>
			</div>
		</div>
		<table class="table table-hover" id="BlockListTable">
			<caption>Danh sách các khu vực trang trại</caption>
			<tr>
				<th>ID</th>
				<th>Tên</th>
				<th>Người quản lí</th>
				<th>Thao tác</th>
			</tr>
		</table>
		<script type="text/javascript">
			$().ready(function() {
				$.ajax({url: '<%=request.getContextPath()%>/api/block/all', 
					success: function(json) {
						for (i = 0; i < json.length; i++) {
							var row = '<tr><td>' + json[i].id + '</td><td>' + json[i].name
								+ '</td><td>' + json[i].manager.fullName + '</td>';
							row += '<td><a href="<%=request.getContextPath() + "/block/view?id="%>' + json[i].id + '">View</a> ';
							row += '<a href="<%=request.getContextPath() + "/block/update?id="%>' + json[i].id + '">Update</a> ';
							row += '<a href="<%=request.getContextPath() + "/api/block/delete?id="%>' + json[i].id + '" onclick="return confirmDelete()">Delete</a></td></tr>';
							$("#BlockListTable").append(row);
						}
			    	}
				});
			});
			function confirmDelete() {
				return confirm('Chắc chắn xoá?');
			}
		</script>
	</div>
	
	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>