<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bán heo</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<div class="row">
			<h1>Bán heo</h1>
			<a href="#" class="btn btn-primary">Bắt đầu giao dịch</a>
		</div>
		<table class="table table-hover" id="TradeTable">
			<caption>Chọn heo để bán</caption>
			<tr>
				<th><input type="checkbox" id="checkAll"></th>
				<th>STT</th>
				<th>Khu</th>
				<th>Chuồng</th>
				<th>ID heo</th>
				<th>Nhập chuồng</th>
				<th>Tên chủng loại</th>
				<th>Cân nặng</th>
				<th>Sức khỏe</th>
				<th>Tăng trưởng</th>
			</tr>
		</table>
		<script type="text/javascript">
			// FIXME
			$().ready(function() {
				var textPigs = '['
					+ '{"blockName": "Khu A", "pigpenId": 1, "pigId": 1, "timestamp": 1476701705000, "raceName": "heo tai xanh", '
											+ '"weight": 94, "rate": "bình thường", "growingStatus": "95%"}, '
					+ '{"blockName": "Khu A", "pigpenId": 2, "pigId": 2, "timestamp": 1476001705000, "raceName": "heo tai xanh", '
											+ '"weight": 90, "rate": "bình thường", "growingStatus": "96%"}, '
					+ '{"blockName": "Khu A", "pigpenId": 3, "pigId": 3, "timestamp": 1476001705000, "raceName": "heo tai xanh", '
											+ '"weight": 95, "rate": "bình thường", "growingStatus": "97%"}, '
					+ '{"blockName": "Khu A", "pigpenId": 4, "pigId": 5, "timestamp": 1476001705000, "raceName": "heo tai xanh", '
											+ '"weight": 100, "rate": "bình thường", "growingStatus": "100%"}, '
					+ '{"blockName": "Khu B", "pigpenId": 11, "pigId": 11, "timestamp": 1476701705000, "raceName": "heo tai xanh", '
											+ '"weight": 94, "rate": "bình thường", "growingStatus": "95%"}, '
					+ '{"blockName": "Khu B", "pigpenId": 12, "pigId": 12, "timestamp": 1476001705000, "raceName": "heo tai xanh", '
											+ '"weight": 90, "rate": "bình thường", "growingStatus": "96%"}, '
					+ '{"blockName": "Khu B", "pigpenId": 13, "pigId": 13, "timestamp": 1476001705000, "raceName": "heo tai xanh", '
											+ '"weight": 95, "rate": "bình thường", "growingStatus": "97%"}, '
					+ '{"blockName": "Khu B", "pigpenId": 14, "pigId": 15, "timestamp": 1476001705000, "raceName": "heo tai xanh", '
											+ '"weight": 100, "rate": "bình thường", "growingStatus": "100%"}'
							+ ']';
				var jsonPigs = JSON.parse(textPigs);
				for (i = 0; i < jsonPigs.length; i++) {
					var STT = i + 1;
					var row = '<tr><td><input type="checkbox"></td><td>' + STT
						+ '</td><td>' + jsonPigs[i].blockName
						+ '</td><td>' + jsonPigs[i].pigpenId
						+ '</td><td>' + jsonPigs[i].pigId
						+ '</td><td>' + new Date(jsonPigs[i].timestamp).toLocaleString()
						+ '</td><td>' + jsonPigs[i].raceName
						+ '</td><td>' + jsonPigs[i].weight
						+ '</td><td>' + jsonPigs[i].rate
						+ '</td><td>' + jsonPigs[i].growingStatus + '</td></tr>';
					$("#TradeTable").append(row);
				}
			});
		</script>
	</div>
	
	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>