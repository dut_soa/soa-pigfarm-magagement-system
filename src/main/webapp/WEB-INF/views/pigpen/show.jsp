<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%
	int blockId = Integer.parseInt(request.getAttribute("blockId").toString());
	%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Heo | Hệ thống quản lí trang trại nuôi heo thịt</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>

<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	<jsp:include page="../../common/_loading.jsp"></jsp:include>

	<div class="container">
		<!--TABS-->
		<ul class="nav nav-tabs" id="blockTabs"></ul>
	</div>
	<div class="container">
		<!-- Bảng thông tin chung -->
		<table class="table table-bordered">
			<tr>
				<td>Người quản lí</td>
				<td id="manager">None</td>
			</tr>
			<tr>
				<td>Số lượng chuồng</td>
				<td id="pigpenCount">0</td>
			</tr>
			<tr>
				<td>Số lượng heo</td>
				<td id="pigCount">0</td>
			</tr>
		</table>
	</div>

	<div class="alert alert-warning">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Chú ý!</strong> Di chuột lên các item, hình ảnh, progressbar
		để biết thêm chi tiết.
	</div>
	<div class="container">
		<!-- DATA WILL BE LOAĐE INTO THIS PLACE-->
		<div class="col-md-6">
			<ul class="list-group" id="leftGroup"></ul>
		</div>
		<div class="col-md-6">
			<ul class="list-group" id="rightGroup"></ul>
		</div>
	</div>
	<!-- /.container -->


	<!-- ================================================== -->
	<!-- Phần script xử lí dữ liệu -->

	<script type="text/javascript">
		$().ready(function() {

			$.ajax({ // AJAX1 - Get Block List
				url: '<%=request.getContextPath() + "/api/block/all" %>', 				
				success: function(jsonBlockList) {
					for (i = 0 ; i < jsonBlockList.length; i++) {
						var item = '<li' + (jsonBlockList[i].id == <%=blockId%> ? ' class="active"' : '') +  '><a href="show?blockId=' +jsonBlockList[i].id+ '">' +jsonBlockList[i].name+ '</a></li>';
						$('#blockTabs').append(item);
					}
		    	}
			}); // .AJAX1
			
			$.ajax({ // AJAX2
				url: '<%=request.getContextPath() + "/api/block/view?id=" + blockId%>', 				
				success: function(jsonBlock) {
					var link = '<a href="<%=request.getContextPath() + "/account/view?id="%>' +jsonBlock.manager.id+ '">' +jsonBlock.manager.fullName+ '</a>';
					$('#manager').html(link);
		    	}
			}); // .AJAX2
			
			$.ajax({ // AJAX3
				url: '<%=request.getContextPath() + "/api/pigpen/all?blockId=" + blockId%>', 				
				success: function(jsonPigpenList, updateCountCallBack) {
					var pigCount = jsonPigpenList.length;
					for (i = 0; i < jsonPigpenList.length; i++) {
						if (jsonPigpenList[i].pig == null)  {
							pigCount--;
						}
					}
					$('#pigCount').html(pigCount);
					$('#pigpenCount').html(jsonPigpenList.length);
					// Danh sách heo cột bên trái
					for (index = 0; index < jsonPigpenList.length / 2; index++) {
						appendData(jsonPigpenList[index], '#leftGroup');
					}
					// Danh sách heo cột bên phải
					for (index = jsonPigpenList.length / 2; index < jsonPigpenList.length; index++) {
						appendData(jsonPigpenList[index], '#rightGroup');
					}
		    	},
		    	complete: function() {
		    		$("#wait").css("display", "none");
		    	}
			}); // .AJAX3
		});
		/**
		 * Hàm nạp dữ liệu từng heo vào giao diện
		 * pigpen_data: đối tượng dữ liệu của chuồng heo
		 * element_id: id của html cần nạp
		 */
		function appendData(pigpen_data, element_id) {
			// Chép template đẩy vào #workingZone
			$('#workingZone').html($('#template').html());
			// Nạp dữ liệu vào item
			if (pigpen_data.pig) { // Chuồng có heo
				$('#workingZone #pigpenId').html(pigpen_data.id);
				$('#workingZone #pigRace').html('<a href="<%=request.getContextPath() + "/race/view?id="%>'+pigpen_data.pig.race.id+'">'+pigpen_data.pig.race.name+'</a>');
				$('#workingZone .media-object').attr('src', Boolean(pigpen_data.pig.gender) ? 
								'<%=request.getContextPath()%>/images/pig_m.png'
								: '<%=request.getContextPath()%>/images/pig_f.png');
				var created_time = new Date(new Number(pigpen_data.pig.createdAt));
				var updated_time = new Date(new Number(pigpen_data.pig.updatedAt));
				$('#workingZone .media').attr('title',
						(Boolean(pigpen_data.pig.gender) ? '[Đực]' : '[Cái]')
								+ '\n Ngày nhập: ' + created_time.getDate()
								+ '/' + (created_time.getMonth() + 1) + '/'
								+ created_time.getFullYear()
								+ '\n Ngày giờ cập nhật: '
								+ updated_time.getHours() + ':'
								+ updated_time.getMinutes() + ' '
								+ updated_time.getDate() + '/'
								+ (updated_time.getMonth() + 1) + '/'
								+ updated_time.getFullYear());
				var current_time = new Date();
				var milisec_offset = new Number( current_time.getTime() - created_time.getTime() );
				var day_offset = Math.floor(milisec_offset / 86400 / 1000);
				var percentage = Math.floor(day_offset / pigpen_data.pig.race.totalGrowingDays * 100);
				$('#workingZone .progress-bar').attr('aria-valuenow',percentage);
				$('#workingZone .progress-bar').attr(
						'title',
						'ngày sinh trưởng: ' + day_offset + '/'
								+ pigpen_data.pig.race.totalGrowingDays
								+ ' (' + percentage + ' %)');
				$('#workingZone .progress-bar').css('width', '' + percentage + '%');
				$('#workingZone .progress-bar').html(percentage + ' %');
				$('#pigHealth').attr('href', '<%=request.getContextPath() + "/health-journal/view"%>');
				//.... định mệnh 1 đống thông tin ở đây chưa làm. làm sau.
				var item = '<li class="list-group-item">'+ $('#workingZone').html() + '</li>';
			} else { // Chuồng ko có heo nào
				$('#workingZone #pigpenId').html(pigpen_data.id);
				$('#workingZone #pigRace').html("Chuồng rỗng");
				$('#workingZone .media-object').attr('src', '<%=request.getContextPath()%>/images/none.png');
				$('#workingZone #actionButtons').html('');
				$('#workingZone .progress').html('');
				var item = '<li class="list-group-item disabled">'
						+ $('#workingZone').html() + '</li>';
			}

			// Đẩy lên giao diện
			$(element_id).append(item);
			// làm rỗng workingZone
			$('#workingZone').html('');
		}
	</script>

	<!-- Khu vực xử lí dữ liệu Javascript -->
	<div id="workingZone" style="display: none;">Xóa là chết!</div>

	<!-- Template for Client-->
	<div style="display: none" id="template">
		<!-- Pigpen item -->
		<div class="media">
			<div class="media-left">
				<img src="<%=request.getContextPath()%>/images/none.png"
					class="media-object" style="width: 60px; height: 60px;">
			</div>
			<div class="media-body">
				<center class="row">
					<div class="btn-group">
						<button class="btn btn-info" type="button">
							Chuồng <span class="badge" id="pigpenId">Mã chuồng</span>
						</button>
						<button class="btn btn-default" type="button">
							<span id="pigRace">Giống heo</span>
						</button>
					</div>
					<div class="btn-group" id="actionButtons">
						<a type="button" class="btn btn-default"
							title="Tình trạng sức khỏe" id="pigHealth"> <i
							class="glyphicon glyphicon-heart"></i>
						</a> <a type="button" class="btn btn-default" title="Xem chi tiết"
							id="btnR"> <i class="glyphicon glyphicon-eye-open"></i>
						</a> <a type="button" class="btn btn-default" title="Sửa thông tin"
							id="btnU"> <i class="glyphicon glyphicon-pencil"></i>
						</a> <a type="button" class="btn btn-danger" title="Xóa heo này"
							id="btnD"> <i class="glyphicon glyphicon-remove"></i>
						</a>
					</div>
				</center>
				<br>
				<div class="progress">
					<div class="progress-bar progress-bar-primary" role="progressbar"
						aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
						style="width: 40%;">N ngày</div>
				</div>
			</div>
		</div>
		<!--/.media -->
	</div>
	<!--/#template -->

	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>
