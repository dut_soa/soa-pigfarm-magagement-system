<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lịch sử theo dõi sức khỏe heo</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<h1>Health Journal</h1>
		<table class="table table-hover" id="HealthJournalTable">
			<caption>Lịch sử theo dõi sức khỏe heo</caption>
			<tr>
				<th>STT</th>
				<th>Thời gian</th>
				<th>Khu</th>
				<th>Chuồng</th>
				<th>Tên chủng loại</th>
				<th>Cân nặng</th>
				<th>Tình trạng sức khỏe</th>
				<th>Ghi chú</th>
				<th>Người theo dõi</th>
			</tr>
		</table>
		<script type="text/javascript">
			// FIXME
			$().ready(function() {
				var textHealthJournals = '[{"timestamp": 1477701705000, ' 
					+ '"blockName": "Khu A", "pigpenId": 1, "raceName": "heo tai xanh", '
					+ '"weight": 20, "rate": "bình thường", "note": "Heo phát triển bình thường", '
					+ '"worker": "Nguyễn Bá Anh Nông Dân"}, '
										+ '{"timestamp": 1477701706000, ' 
					+ '"blockName": "Khu A", "pigpenId": 2, "raceName": "heo tai xanh", '
					+ '"weight": 21, "rate": "bình thường", "note": "Heo phát triển bình thường", '
					+ '"worker": "Nguyễn Bá Anh Nông Dân"}, '
										+ '{"timestamp": 1477701707000, ' 
					+ '"blockName": "Khu A", "pigpenId": 3, "raceName": "heo tai xanh", '
					+ '"weight": 19, "rate": "bình thường", "note": "Heo phát triển bình thường", '
					+ '"worker": "Nguyễn Bá Anh Nông Dân"}, '
										+ '{"timestamp": 1477701708000, ' 
					+ '"blockName": "Khu A", "pigpenId": 5, "raceName": "heo tai xanh", '
					+ '"weight": 23, "rate": "bình thường", "note": "Heo phát triển bình thường", '
					+ '"worker": "Nguyễn Bá Anh Nông Dân"}, '
										+ '{"timestamp": 1477601718000, ' 
					+ '"blockName": "Khu A", "pigpenId": 1, "raceName": "heo tai xanh", '
					+ '"weight": 18, "rate": "hơi xấu", "note": "Heo có biểu hiện kén ăn", '
					+ '"worker": "Nguyễn Bá Anh Nông Dân"}, '
										+ '{"timestamp": 1477701706000, ' 
					+ '"blockName": "Khu B", "pigpenId": 12, "raceName": "heo tai đỏ", '
					+ '"weight": 33, "rate": "bình thường", "note": "Heo phát triển bình thường", '
					+ '"worker": "Chị quản lý"}, '
										+ '{"timestamp": 1477701707000, ' 
					+ '"blockName": "Khu B", "pigpenId": 14, "raceName": "heo tai đỏ", '
					+ '"weight": 34, "rate": "bình thường", "note": "Heo phát triển bình thường", '
					+ '"worker": "Chị quản lý"}, '
										+ '{"timestamp": 1477701708000, ' 
					+ '"blockName": "Khu B", "pigpenId": 15, "raceName": "heo tai đỏ", '
					+ '"weight": 33, "rate": "bình thường", "note": "Heo phát triển bình thường", '
					+ '"worker": "Chị quản lý"}, '
										+ '{"timestamp": 1477601718000, ' 
					+ '"blockName": "Khu B", "pigpenId": 16, "raceName": "heo tai đỏ", '
					+ '"weight": 32, "rate": "bình thường", "note": "Heo phát triển bình thường", '
					+ '"worker": "Chị quản lý"}]';
				var jsonHealthJournals = JSON.parse(textHealthJournals);
				for (i = 0; i < jsonHealthJournals.length; i++) {
					var STT = i + 1;
					var row = '<tr><td>' + STT
						+ '</td><td>' + new Date(jsonHealthJournals[i].timestamp).toLocaleString()
						+ '</td><td>' + jsonHealthJournals[i].blockName
						+ '</td><td>' + jsonHealthJournals[i].pigpenId
						+ '</td><td>' + jsonHealthJournals[i].raceName
						+ '</td><td>' + jsonHealthJournals[i].weight
						+ '</td><td>' + jsonHealthJournals[i].rate
						+ '</td><td>' + jsonHealthJournals[i].note
						+ '</td><td>' + jsonHealthJournals[i].worker + '</td></tr>';
					$("#HealthJournalTable").append(row);
				}
			});
		</script>
	</div>
	
	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>