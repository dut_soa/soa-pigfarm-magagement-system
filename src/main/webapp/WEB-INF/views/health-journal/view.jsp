<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tình trạng sức khỏe heo</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<h1>Health Journal</h1>
		
		<table class="table table-hover" id="HealthTable">
			<caption>Tình trạng sức khỏe heo</caption>
			<tr>
				<td>Khu</td>
				<td><h5>Khu A</h5></td>
			</tr>
			<tr>
				<td>Chuồng</td>
				<td>1</td>
			</tr>
			<tr>
				<td>ID heo</td>
				<td>1</td>
			</tr>
			<tr>
				<td>Tên chủng loại</td>
				<td>heo tai xanh</td>
			</tr>
			<tr>
				<td>Cân nặng hiện tại</td>
				<td>20</td>
			</tr>
			<tr>
				<td>Sức khỏe hiện tại</td>
				<td>Bình thường</td>
			</tr>
			<tr>
				<td>Ghi chú</td>
				<td>Heo phát triển bình thường</td>
			</tr>
			<tr>
				<td>Lần cập nhật cuối cùng</td>
				<td>07:41:45, 29/10/2016</td>
			</tr>
			<tr>
				<td>Người cập nhật</td>
				<td>Nguyễn Bá Anh Nông Dân</td>
			</tr>
		</table>
		<div class="row">
			<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Xem lịch sử theo dõi</button>
			<div id="demo" class="collapse">
				<table class="table table-hover" id="DetailJournalTable">
					<caption>Lịch sử theo dõi chi tiết</caption>
					<tr>
						<th>STT</th>
						<th>Thời gian</th>
						<th>Cân nặng</th>
						<th>Tình trạng sức khỏe</th>
						<th>Ghi chú</th>
						<th>Người theo dõi</th>
					</tr>
				</table>
			</div>
		</div>
		<script type="text/javascript">
			// FIXME
			$().ready(function() {
				var textHealthJournals = '[{"timestamp": 1477701705000, ' 
					+ '"blockName": "Khu A", "pigpenId": 1, "raceName": "heo tai xanh", '
					+ '"weight": 20, "rate": "bình thường", "note": "Heo phát triển bình thường", '
					+ '"worker": "Nguyễn Bá Anh Nông Dân"}, '
										+ '{"timestamp": 1477601718000, ' 
					+ '"blockName": "Khu A", "pigpenId": 1, "raceName": "heo tai xanh", '
					+ '"weight": 18, "rate": "hơi xấu", "note": "Heo có biểu hiện kén ăn", '
					+ '"worker": "Nguyễn Bá Anh Nông Dân"}]';
				var jsonHealthJournals = JSON.parse(textHealthJournals);
				for (i = 0; i < jsonHealthJournals.length; i++) {
					var STT = i + 1;
					var row = '<tr><td>' + STT
						+ '</td><td>' + new Date(jsonHealthJournals[i].timestamp).toLocaleString()
						+ '</td><td>' + jsonHealthJournals[i].weight
						+ '</td><td>' + jsonHealthJournals[i].rate
						+ '</td><td>' + jsonHealthJournals[i].note
						+ '</td><td>' + jsonHealthJournals[i].worker + '</td></tr>';
					$("#DetailJournalTable").append(row);
				}
			});
		</script>
	</div>
	
	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>