<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	/* Common form for Create and Update actions */
	boolean isNewRecord = true;
	int id = 0;
	try {
		id = Integer.parseInt(request.getAttribute("id").toString());
		isNewRecord = (id <= 0) ? true : false;
	} catch (NumberFormatException e) {} catch(NullPointerException e) {}
	
	String formAction = isNewRecord ? request.getContextPath() + "/api/account/create" : request.getContextPath() + "/api/account/update";
	String formMethod = isNewRecord ? "post" : "post"; // 2nd: PUT
	String submitButtonLabel = isNewRecord ? "Tạo mới" : "Cập nhật";
	String hiddenId = isNewRecord ? ("") : ("<input type=\"hidden\" name=\"id\" value=\"" + id +"\">");
	
	try {
		String token = session.getAttribute("token").toString();
		hiddenId += "<input type=\"hidden\" name=\"token\" value=\"" + token +"\">";		
	} catch (NullPointerException e) {}
	
%>
<script>
	
</script>
<form class="form form-horizontal" id="AccountForm" name="AccountForm" method="<%=formMethod %>" action="<%=formAction %>">
	<%=hiddenId %>
	<div class="form-group">
		<label class="col-md-3 control-label">Tài khoản <span
			class="text text-danger">*</span></label>
		<div class="col-md-9">
			<input type="text" name="username" id="username" class="form-control" <%=isNewRecord?"":"readonly" %>>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 control-label">Mật khẩu <span
			class="text text-danger">*</span></label>
		<div class="col-md-9">
			<input type="password" name="password" id="password"
				class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 control-label">Nhập lại mật khẩu <span
			class="text text-danger">*</span></label>
		<div class="col-md-9">
			<input type="password" name="confirmedPassword"
				id="confirmedPassword" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 control-label">Họ và tên <span
			class="text text-danger">*</span></label>
		<div class="col-md-9">
			<input type="text" name="fullName" id="fullName" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 control-label">Số điện thoại <span
			class="text text-danger">*</span></label>
		<div class="col-md-9">
			<input type="text" name="phoneNumber" id="phoneNumber"
				class="form-control">
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-md-3 control-label">Email <span
			class="text text-danger">*</span></label>
		<div class="col-md-9">
			<input type="email" name="email" id="email"
				class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 control-label">Địa chỉ</label>
		<div class="col-md-9">
			<input type="text" name="address" id="address" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 control-label">Chức vụ</label>
		<div class="col-md-9">
			<select name="role" id="role" class="form-control">
				<option value="worker">Nhân công</option>
				<option value="manager">Quản lí</option>
				<option value="admin">Quản trị</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<a href="<%=request.getContextPath() + "/account"%>" class="btn btn-default btn-block">
				 <i class="glyphicon glyphicon-triangle-left"></i> Index </a>
		</div>
		<div class="col-md-offset-4 col-md-2">
			<button type="reset" class="btn btn-warning btn-block">Reset</button>
		</div>
		<div class="col-md-2">
			<button type="submit" class="btn btn-success btn-block"><%=submitButtonLabel %></button>
		</div>
	</div>
</form>

<jsp:include page="../../common/_jquery_validator.jsp"></jsp:include>
<script type="text/javascript">
	$.validator.addMethod( "phoneValidate", function( phone_number, element ) {
	    phone_number = phone_number.replace( /\s+/g, "" );
	    return this.optional( element ) || phone_number.length > 10 && phone_number.match( /^(\+{1})\d+$/ );
	    }, "Nhập đúng số điện thoại cái cha nội!" // trollol-face!
	  );

	$().ready(function() {
		$('#AccountForm').validate({
			rules : {
				username : {
					required : true,
					maxlength : 50
				},
				password : {
					required : true,
					maxlength : 50
				},
				confirmedPassword : {
					required : true,
					maxlength : 50
				},
				fullName : {
					required : true,
					maxlength : 100
				},
				email : {
					email : true,
					maxlength : 100
				},
				phoneNumber : {
					required : true,
					//phoneValidate : true
				},
				address : {
					required : false,
					maxlength : 100
				}
			},
	        errorElement: "em",
	        errorPlacement: function (error, element) {
	            // Add the `help-block` class to the error element
	            error.addClass("help-block");
				error.insertAfter(element);
	        },
	        highlight: function (element, errorClass, validClass) {
	            $(element).closest(".form-group").addClass("has-error").removeClass("has-success");
	        },
	        unhighlight: function (element, errorClass, validClass) {
	            $(element).closest(".form-group").removeClass("has-error");
	        }
		}); // .validate()
		
		$.ajax({url: '<%=request.getContextPath() + "/api/account/view?id=" + id%>', 
			success: function(json) {
				if (json.error) {
					
				} else {
					$('#username').val(json.username);
					$('#fullName').val(json.fullName);
					$('#phoneNumber').val(json.phoneNumber);
					$('#address').val(json.address);
					$('#email').val(json.email);
					$('#role option[value=' + json.role + ']').prop('selected', true);
				}
	    	}
		}); // .ajax()
		
		$('#AccountForm').submit(function(event) {
			if (isCreateForm) {
				$.ajax({
			           url: '<%=request.getContextPath() + "/api/account/login"%>',
			           type: 'POST',
			           data: $('#LoginForm').serialize(),
			           success: function(json){
			        	   if (json.error) {
			        		   alert(json.error + ': ' + json.message);
			        	   } else {
			        		   $.ajax({
			    		           url: '<%=request.getContextPath() + "/api/account/login"%>',
			    		           type: 'POST',
			    		           data: $('#LoginForm').serialize(),
			    		           success: function(json){
			    		        	   if (json.error) {
			    		        		   alert(json.error + ': ' + json.message);
			    		        	   } else {
			    		        		   alert('Tạo tài khoản thành công. Token : ' + json.authToken);
			    		        	   }
			    		               
			    		           }
			    		         });
			    		    event.preventDefault();
			        		   alert('Đăng nhập thành công. Token : ' + json.authToken);
			        	   }
			           }
			         });
			    event.preventDefault();	
			}
		});
		
	});
</script>