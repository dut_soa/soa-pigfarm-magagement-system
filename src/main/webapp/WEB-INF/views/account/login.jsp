<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Đăng nhập</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Đăng nhập</h1>
            <div class="account-wall">
                <img class="profile-img" src="<%=request.getContextPath()%>/images/avatar.jpg"
                    alt="">
                <form class="form-signin" name="LoginForm" id="LoginForm">
                <input type="text" class="form-control" name="username" id="username" required autofocus>
                <input type="password" class="form-control" type="password" name="password" id="password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">
                    Đăng nhập</button>
                <label class="checkbox pull-left">
                    <input type="checkbox" value="remember-me">
                    Ghi nhớ
                </label>
                <a href="#" class="pull-right need-help">Quên mật khẩu? </a><span class="clearfix"></span>
                </form>
            </div>
        </div>
    </div>
</div>


<!--
<form class="form form-horizontal" id="LoginForm" name="LoginForm" method="post">
	<div class="form-group">
		<label class="col-sm-3 control-label">Tài khoản<span
			class="text text-danger">*</span></label>
		<div class="col-sm-8">
			<input type="text" name="username" id="username" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">Mật khẩu<span
			class="text text-danger">*</span></label>
		<div class="col-sm-8">
			<input type="password" name="password" id="password"
				class="form-control">
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-offset-3 col-sm-3">
			<button type="reset" class="btn btn-warning btn-block">Reset</button>
		</div>
		<div class="col-sm-3">
			<button type="submit" class="btn btn-success btn-block">Đăng nhập</button>
		</div>
	</div>
</form>
-->

<jsp:include page="../../common/_jquery_validator.jsp"></jsp:include>
<script type="text/javascript">
	$().ready(function() {
		$('#LoginForm').validate({
			rules : {
				username : {
					required : true,
					maxlength : 50
				},
				password : {
					required : true,
					maxlength : 50
				}
			},
			errorClass : 'text text-danger'
		});
		//==================
		$('#LoginForm').submit(function(event) {
		    $.ajax({
		           url: '<%=request.getContextPath() + "/api/account/login"%>',
		           type: 'POST',
		           data: $('#LoginForm').serialize(),
		           success: function(json){
		        	   if (json.error) {
		        		   alert(json.error + ': ' + json.message);
		        	   } else {
		        		   $('#hiddenItem').html(json.authToken);
		        		   $.ajax({ // ajax2
								url: '<%=request.getContextPath() + "/session?action=setToken&token="%>' + json.authToken, //$('#hiddenItem').html(), 
		        				success: function(json) {
		        					if (json.error) {
		        						alert(json.error)
		        					} else if (json.success) {
		        						alert('Đăng nhập thành công!');
		        						window.location.replace('<%=request.getContextPath() + "/pigpen/show?blockId=1"%>');
		        					}
		        		    	}
		        			}); // .ajax2		
		        	   }
		           }
		         });
		    event.preventDefault();
		});
	});
</script>
</div>
</body>
</html>