<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Đổi mật khẩu</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="../../common/_navbar.jsp"></jsp:include>

<div class="container">
<%
	int id = 0;
	try {
		 id = Integer.parseInt(request.getAttribute("id").toString());
	} catch (NumberFormatException e) {} catch(NullPointerException e) {}
	String hiddenId = id <= 0 ? ("") : ("<input type=\"hidden\" name=\"id\" value=\"" + id +"\">");
%>
<form class="form form-horizontal" id="PasswordForm" name="AccountForm" method="post" action="<%=request.getContextPath() + "/api/account/change-password"%>">
	<%=hiddenId %>
	<div class="form-group">
		<label class="col-md-3 control-label">Mật khẩu cũ <span
			class="text text-danger">*</span></label>
		<div class="col-md-9">
			<input type="password" name="oldPassword" id="oldPassword" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 control-label">Mật khẩu mới<span
			class="text text-danger">*</span></label>
		<div class="col-md-9">
			<input type="password" name="newPassword" id="newPassword"
				class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 control-label">Nhập lại mật khẩu <span
			class="text text-danger">*</span></label>
		<div class="col-md-9">
			<input type="password" name="confirmedPassword"
				id="confirmedPassword" class="form-control">
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<a href="<%=request.getContextPath() + "/account/view?id=" + id %>" class="btn btn-default btn-block">
				 <i class="glyphicon glyphicon-triangle-left"></i> View </a>
		</div>
		<div class="col-md-offset-4 col-md-2">
			<button type="reset" class="btn btn-warning btn-block">Reset</button>
		</div>
		<div class="col-md-2">
			<button type="submit" class="btn btn-success btn-block">Đổi mật khẩu</button>
		</div>
	</div>
</form>

<jsp:include page="../../common/_jquery_validator.jsp"></jsp:include>
<script type="text/javascript">
	$().ready(function() {
		$('#PasswordForm').validate({
			rules : {
				oldPassword : {
					required : true,
					maxlength : 50
				},
				newPassword : {
					required : true,
					maxlength : 50
				},
				confirmedPassword : {
					required : true,
					maxlength : 50
				}
			},
			errorClass : 'text text-danger'
		});
		
	});
</script>
</div>


<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>