<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%
	int id = Integer.parseInt(request.getAttribute("id").toString());
	%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Xem tài khoản</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<h1 id="heading"></h1>
		<table class="table table-hover" id="AccountListTable">
			<caption>Thông tin chi tiết</caption>
			<tr>
				<td>ID</td>
				<td id="id"></td>
			</tr>
			<tr>
				<td>Tài khoản</td>
				<td id="username"></td>
			</tr>
			<tr>
				<td>Họ tên</td>
				<td id="fullName"></td>
			</tr>
			<tr>
				<td>Địa chỉ</td>
				<td id="address"></td>
			</tr>
			<tr>
				<td>Số ĐT</td>
				<td id="phoneNumber"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td id="email"></td>
			</tr>
			<tr>
				<td>Chức vụ</td>
				<td id="role"></td>
			</tr>
		</table>
		<div class="row">
			<div class="col-md-2">
				<a href="<%=request.getContextPath() + "/account" %>" class="btn btn-default btn-block">
					 <i class="glyphicon glyphicon-triangle-left"></i> Index </a>
			</div>
			<div class="col-md-2">
				<a href="" id="changePasswordLink" class="btn btn-block btn-warning">
					<i class="glyphicon glyphicon-pencil"></i> Đổi mật khẩu
				</a>
			</div>
			<div class="col-md-2">
				<a href="" id="updateLink" class="btn btn-block btn-success">
					<i class="glyphicon glyphicon-pencil"></i> Cập nhật
				</a>
			</div>
			<div class="col-md-2">
				<a href="#" id="deleteLink" class="btn btn-block btn-danger" onClick="return confirmDelete()">
					<i class="glyphicon glyphicon-trash"></i> Xóa
				</a>
			</div>
		</div>
		<script>
			$().ready(function() {
				$.ajax({url: '<%=request.getContextPath()%>/api/account/view?id=<%=id%>', 
					success: function(json) {
						if (!json.error) {
							$('#heading').html(json.id + ': ' + json.username);
							$('#id').html(json.id);
							$('#username').html(json.username);
							$('#fullName').html(json.fullName);
							$('#address').html(json.address);
							$('#phoneNumber').html(json.phoneNumber);
							$('#email').html(json.email);
							$('#role').html(json.role);
							$('#changePasswordLink').attr('href', '<%=request.getContextPath() + "/account/change-password?id=" %>' + json.id);
							$('#updateLink').attr('href', '<%=request.getContextPath() + "/account/update?id=" %>' + json.id);
							$('#deleteLink').attr('href', '<%=request.getContextPath() + "/api/account/delete?id=" %>' + json.id);
							if (json.deleted_at > 0) {
								$('.alert').html('Tài khỏan này đã bị xóa!');
							} else {
								$('.alert').remove();
							}
						} else {
							alert ('Lỗi !');
						}
			    	}
				});
			});
			function confirmDelete() {
				return confirm('Chắc chắn xoá?');
			}
		</script>
	</div>
	
	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>