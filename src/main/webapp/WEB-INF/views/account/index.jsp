<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách tài khoản</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	<jsp:include page="../../common/_loading.jsp"></jsp:include>
	
	<div class="container">
		<h1>Account</h1>
		<div class="row">
			<div class="col-md-2">
				<a href="<%=request.getContextPath() + "/account/create" %>" class="btn btn-block btn-success">Tạo mới</a>
			</div>
		</div>
		<table class="table table-hover" id="AccountListTable">
			<caption>Danh sách tài khoản</caption>
			<tr>
				<th>ID</th>
				<th>Tài khoản</th>
				<th>Họ tên</th>
				<th>Số ĐT</th>
				<th>Quyền hạn</th>
				<th>Thao tác</th>
			</tr>
		</table>
		<script type="text/javascript">
			$().ready(function() {
				$.ajax({url: '<%=request.getContextPath()%>/api/account/all', 
					success: function(json) {
						for (i = 0; i < json.length; i++) {
							var row = '<tr><td>' + json[i].id + '</td><td>' + json[i].username
								+ '</td><td>' + json[i].fullName + '</td><td>' + json[i].phoneNumber
								+ '</td><td>' + json[i].role + '</td>';
							row += '<td><a href="<%=request.getContextPath() + "/account/view?id="%>' + json[i].id + '">View</a> ';
							row += '<a href="<%=request.getContextPath() + "/account/update?id="%>' + json[i].id + '">Update</a> ';
							row += '<a href="<%=request.getContextPath() + "/api/account/delete?id="%>' + json[i].id + '" onclick="return confirmDelete()">Delete</a></td></tr>';
							$("#AccountListTable").append(row);
						}
			    	},
			    	complete: function() {
			    		$("#wait").css("display", "none");
			    	}
				});
			});
			function confirmDelete() {
				return confirm('Chắc chắn xoá?');
			}
		</script>
	</div>
	
	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>