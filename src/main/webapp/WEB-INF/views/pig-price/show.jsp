<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bảng giá heo hiện tại</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<h1>Pig price</h1>
		<div class="row">
			<div class="col-sm-2">
				<a href="<%=request.getContextPath() + "/pig-price" %>" class="btn btn-success">Xem lịch sử bảng giá</a>
			</div>
		</div>
		<table class="table table-hover" id="CurrentPigPriceTable">
			<caption>Bảng giá heo hiện tại</caption>
			<tr>
				<th>ID chủng loại</th>
				<th>Tên chủng loại</th>
				<th>Giá (nghìn VNĐ/kg)</th>
				<th>Lần cập nhật gần nhất</th>
				<th>Thao tác</th>
			</tr>
		</table>
		<script type="text/javascript">
			$().ready(function() {
				$.ajax({url: '<%=request.getContextPath()%>/api/pig-price/show', 
					success: function(jsonPigPrices) {
						for (i = 0; i < jsonPigPrices.length; i++) {
							var row = '<tr><td>' + jsonPigPrices[i].race.id
								+ '</td><td>' + jsonPigPrices[i].race.name
								+ '</td><td>' + jsonPigPrices[i].value
								+ '</td><td>' + new Date(jsonPigPrices[i].timestamp).toLocaleString() + '</td>';
							row += '<td><a href="<%=request.getContextPath() + "/pig-price/update?id="%>' + jsonPigPrices[i].race.id + '">Cập nhật giá mới</a></td></tr>';
							$("#CurrentPigPriceTable").append(row);
						}
			    	}
				});
			});
		</script>
	</div>
	
	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>