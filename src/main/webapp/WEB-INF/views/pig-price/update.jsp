<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update Race</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<% 
int id = 0;
try {
	id = Integer.parseInt(request.getAttribute("id").toString());
} catch (NumberFormatException e) {} catch(NullPointerException e) {}
%>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<h1>Cập nhật giá mới</h1>
		<form class="form form-horizontal" id="PigPriceForm" name="PigPriceForm" method="post"
			action="<%=request.getContextPath() + "/pig-price/show"%>">
			
			<input id="id" type="hidden" name="id" value="<%=id%>">
			
			<div class="form-group">
				<label class="col-sm-3 control-label">ID chủng loại </label>
				<div class="col-sm-9">
					<input type="text" name="race_id" id="race_id" class="form-control" readonly>
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-sm-3 control-label">Tên chủng loại </label>
				<div class="col-sm-9">
					<input type="text" name="race_name" id="race_name" class="form-control" readonly>
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-sm-3 control-label">Giá hiện tại </label>
				<div class="col-sm-9">
					<input type="text" name="current_price" id="current_price" class="form-control" readonly>
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-sm-3 control-label">Giá mới </label>
				<div class="col-sm-9">
					<input type="text" name="new_price" id="new_price" class="form-control">
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-2">
					<a href="<%=request.getContextPath() + "/pig-price/show"%>" class="btn btn-default btn-block">
						 <i class="glyphicon glyphicon-triangle-left"></i> Index </a>
				</div>
				<div class="col-sm-offset-1 col-sm-2">
					<button id="submit" type="submit" class="btn btn-success btn-block">Cập nhật giá</button>
				</div>
			</div>
		</form>
		
		<jsp:include page="../../common/_jquery_validator.jsp"></jsp:include>
		<script type="text/javascript">
			$().ready(function() {
				$('#PigPriceForm').validate({
					rules : {
						new_price : {
							required : true,
							number : true,
							min : 0
						}
					},
					messages: {
						new_price : {
							min : "Số không được nhỏ hơn 0"
						},
			        },
			        errorElement: "em",
			        errorPlacement: function (error, element) {
			            // Add the `help-block` class to the error element
			            error.addClass("help-block");
						error.insertAfter(element);
			        },
			        highlight: function (element, errorClass, validClass) {
			            $(element).closest(".form-group").addClass("has-error").removeClass("has-success");
			        },
			        unhighlight: function (element, errorClass, validClass) {
			            $(element).closest(".form-group").removeClass("has-error");
			        }
				}); // .validate()
				
				$.ajax({url: '<%=request.getContextPath() + "/api/pig-price/view?id=" + id%>',
					success: function(jsonPigPrice) {
						if (jsonPigPrice.error) {
							alert(jsonPigPrice.error);
						} else {
							$("#race_id").val(jsonPigPrice.race.id);
							$("#race_name").val(jsonPigPrice.race.name);
							$("#current_price").val(jsonPigPrice.value);
						}
			    	}
				}); // .ajax()
				
			});
		</script>
	</div>

	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>