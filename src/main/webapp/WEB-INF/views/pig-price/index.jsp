<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lịch sử bảng giá heo</title>
<jsp:include page="../../common/_header.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="../../common/_navbar.jsp"></jsp:include>
	
	<div class="container">
		<h1>Pig price histories</h1>
		<div class="row">
			<div class="col-sm-2">
				<a href="<%=request.getContextPath() + "/pig-price/show" %>" class="btn btn-primary">Xem bảng giá hiện tại</a>
			</div>
		</div>
		<table class="table table-hover" id="FullPigPriceTable">
			<caption>Lịch sử bảng giá heo</caption>
			<tr>
				<th>STT</th>
				<th>Thời gian cập nhật giá</th>
				<th>ID chủng loại</th>
				<th>Tên chủng loại</th>
				<th>Giá (nghìn VNĐ/kg)</th>
			</tr>
		</table>
		<script type="text/javascript">
			$().ready(function() {
				$.ajax({url: '<%=request.getContextPath()%>/api/pig-price/all', 
					success: function(jsonPigPrices) {
						for (i = 0; i < jsonPigPrices.length; i++) {
							var STT = i + 1;
							var row = '<tr><td>' + STT
								+ '</td><td>' + new Date(jsonPigPrices[i].timestamp).toLocaleString()
								+ '</td><td>' + jsonPigPrices[i].race.id
								+ '</td><td>' + jsonPigPrices[i].race.name
								+ '</td><td>' + jsonPigPrices[i].value + '</td></tr>';
							$("#FullPigPriceTable").append(row);
						}
			    	}
				});
			});
		</script>
	</div>
	
	<jsp:include page="../../common/_footer.jsp"></jsp:include>
</body>
</html>