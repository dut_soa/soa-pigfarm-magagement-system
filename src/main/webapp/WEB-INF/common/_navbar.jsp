<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<%=request.getContextPath()%>">Hệ hống quản lí trang trại heo thịt</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<%=request.getContextPath() + "/account"%>">Account</a></li>
      </ul>
      <ul class="nav navbar-nav">
        <li><a href="<%=request.getContextPath() + "/administrator"%>">Administrator</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<%=request.getContextPath() + "/pigpen/show?blockId=1"%>">Heo</a></li>
        <li><a href="<%=request.getContextPath() + "/race"%>">Giống Heo</a></li>
        <li><a href="<%=request.getContextPath() + "/health-journal"%>">Theo dõi sức khỏe heo</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Quản lý bán heo <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<%=request.getContextPath() + "/trade"%>">Bán heo</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<%=request.getContextPath() + "/pig-price/show"%>">Bảng giá</a></li>
          </ul>
        </li>
        
        <%
        	String accountFullName = "";
    		int accountId = 0;
        	try {
        		accountFullName = session.getAttribute("fullName").toString();
        		accountId = Integer.parseInt(session.getAttribute("accountId").toString());
        	} catch (NullPointerException e) {}
        	if (accountId > 0) {
        %>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          	<%=session.getAttribute("fullName") %> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<%=request.getContextPath() + "/account/view?id=" + accountId %>">Profile</a></li>
            <li><a href="<%=request.getContextPath() + "/account/change-password?id=" + accountId%>">Đổi mật khẩu</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<%=request.getContextPath() + "/session?action=destroy"%>">Đăng xuất</a></li>
          </ul>
        </li>
        <% } else { // Show login link %>
        <div id="navbar" class="collapse navbar-collapse pull-right">
          <ul class="nav navbar-nav">
            <li><a href="<%=request.getContextPath() + "/login"%>">Đăng nhập</a></li>
            <li><a href="contact.html">Liên hệ</a></li>
          </ul>
        </div>
        <% } %>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>