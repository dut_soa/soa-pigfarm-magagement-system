<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!-- jQUERY -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-3.1.1.min.js"></script>

<!-- BOOTSTRAP -->
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/bootstrap.min.css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/style.css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/bootstrap-theme.min.css">
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/js.cookie.js"></script>
	
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/loading.css">