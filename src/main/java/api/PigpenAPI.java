/**
 * 
 */
package api;

import java.io.IOException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import dao.PigpenDAO;
import models.Pigpen;

/**
 * @author ngbaanh
 *
 */
@Path("/pigpen")
public class PigpenAPI {
	PigpenDAO pigpenDAO;
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAll(@QueryParam("blockId") int blockId) {
		pigpenDAO = new PigpenDAO();
		ArrayList<Pigpen> list = blockId > 0 ? pigpenDAO.getAllInBlock(blockId) : pigpenDAO.getAll();
		JSONObject jsonObj = new JSONObject();
		try {
			if (list.isEmpty()) {
				jsonObj.put("tag", "view-all-pigpen");
				jsonObj.put("error", "empty");
				jsonObj.put("message", "There is not any pigpen was found in the system");
				return jsonObj.toString();
			} else {
				ObjectMapper om = new ObjectMapper();
				// remove Block info may be duplicated in JSON
				for (Pigpen p : list) {
					p.setBlock(null);
				}
				String json = om.writeValueAsString(list);
				return json;
			}
		} catch (IOException e) {
		} catch (JSONException e) {
		}
		return jsonObj.toString();
	}
	
	@GET
	@Path("/view")
	@Produces(MediaType.APPLICATION_JSON)
	public String get(@QueryParam("id") int id) {
		pigpenDAO  = new PigpenDAO();
		Pigpen pigpen = pigpenDAO.get(id);
		try {
			if (pigpen != null) {
				return new ObjectMapper().writeValueAsString(pigpen);
			} else {
				JSONObject json = new JSONObject();
				json.put("tag", "view-block");
				json.put("error", "not-found");
				json.put("message", "The Block which has ID=" + id + " is not found in the system");
				return json.toString();
			}
		} catch (Exception e) {
		}
		return null;
	}
}
