package api;

import java.io.IOException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import dao.AccountDAO;
import dao.Database;
import models.Account;

@Path("/account")
public class AccountAPI {
	AccountDAO accountDAO;

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllProducts() {
		accountDAO = new AccountDAO();
		ArrayList<Account> list = accountDAO.getAllAccounts();
		JSONObject jsonObj = new JSONObject();
		try {
			if (list.isEmpty()) {
				jsonObj.put("tag", "view-all-account");
				jsonObj.put("error", "empty");
				jsonObj.put("message", "There is not any account was found in the system");
				return jsonObj.toString();
			} else {
				ObjectMapper om = new ObjectMapper();
				String json = om.writeValueAsString(list);
				return json;
			}
		} catch (IOException e) {
		} catch (JSONException e) {
		}
		return jsonObj.toString();
	}

	@GET
	@Path("/view")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAccount(@QueryParam("id") int id) {
		accountDAO = new AccountDAO();
		Account acc = accountDAO.getAccount(id);
		try {
			if (acc != null) {
				return new ObjectMapper().writeValueAsString(acc);
			} else {
				JSONObject json = new JSONObject();
				json.put("tag", "view-account");
				json.put("error", "not-found");
				json.put("message", "The account which has ID=" + id + " is not found in the system");
				return json.toString();
			}
		} catch (Exception e) {
		}
		return null;
	}

	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public String createAccount(
			@FormParam("token") String token,
			@FormParam("username") String username, @FormParam("password") String password,
			@FormParam("confirmedPassword") String confirmedPassword, @FormParam("fullName") String fullName,
			@FormParam("phoneNumber") String phoneNumber, @FormParam("address") String address,
			@FormParam("email") String email, @FormParam("role") String role) {
		JSONObject json = new JSONObject();
		accountDAO = new AccountDAO(Database.getRole(token));
		Account newAccount = new Account(0, username, password, 0, fullName, phoneNumber, email, address, role, null);
		try {
			if (password != null && confirmedPassword != null && !password.equals(confirmedPassword)) {
				json.put("tag", "create-account");
				json.put("error", "password-not-match");
				json.put("message", "Password and Confirmed Password were not matched");
				return json.toString();
			} else if (accountDAO.isUsernameUnique(newAccount)) {
				json.put("tag", "create-account");
				json.put("error", "creation-failed");
				json.put("message", "Creation operation was failed because the Username has been taken before");
				return json.toString();
			} else {
				int newAccountId = accountDAO.createAccount(newAccount);
				if (newAccountId > 0) {
					return this.getAccount(newAccountId);
				} else {
					json.put("tag", "create-account");
					json.put("error", "creation-failed");
					json.put("message", "Creation operation was failed when adding new Account into the system");
					return json.toString();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json.toString();
	}

	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateAccount(@FormParam("token") String token,
			@FormParam("id") int id, @FormParam("username") String username,
			@FormParam("password") String password, @FormParam("confirmedPassword") String confirmedPassword,
			@FormParam("fullName") String fullName, @FormParam("phoneNumber") String phoneNumber,
			@FormParam("address") String address, @FormParam("email") String email, @FormParam("role") String role) {
		System.out.println("Starting updating...");// FIXME
		JSONObject json = new JSONObject();
		accountDAO = new AccountDAO(Database.getRole(token));
		Account newAccount = new Account(id, username, password, 0, fullName, phoneNumber, email, address, role, null);
		try {
			if (password != null && confirmedPassword != null && !password.equals(confirmedPassword)) {
				json.put("tag", "update-account");
				json.put("error", "password-not-match");
				json.put("message", "Password and Confirmed Password were not matched");
				return json.toString();
			} else {
				if (accountDAO.updateAccount(newAccount)) {
					return this.getAccount(id);
				} else {
					json.put("tag", "update-account");
					json.put("error", "update-failed");
					json.put("message", "Update operation was failed when saving Account into the system");
					return json.toString();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json.toString();
	}

	@GET
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateAccount(@QueryParam("token") String token, @QueryParam("id") int id) {
		JSONObject json = new JSONObject();
		accountDAO = new AccountDAO(Database.getRole(token));
		try {
			if (accountDAO.deleteAccount(id)) {
				json.put("tag", "delete-account");
				json.put("success", "delete-success");
				json.put("message", "Account was deleted!");
				return json.toString();
			} else {
				json.put("tag", "delete-account");
				json.put("error", "delete-failed");
				json.put("message", "Delete operation was failed when remove Account from the system");
				return json.toString();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json.toString();
	}

	@POST
	@Path("/change-password")
	@Produces(MediaType.APPLICATION_JSON)
	public String changePassword(@FormParam("id") int id, 
			@FormParam("oldPassword") String oldPassword,
			@FormParam("newPassword") String newPassword, 
			@FormParam("confirmedPassword") String confirmedPassword,
			@FormParam("token") String token) {
		JSONObject json = new JSONObject();
		try {
			if ("".equals(token)) {
				json.put("tag", "update-account");
				json.put("error", "token-not-match");
				json.put("message", "Authentication Token is not matched with your account now. Please login again.");
				return json.toString();
			}
			
			accountDAO = new AccountDAO(Database.getAccessRole(id));
			
			if (oldPassword != null && newPassword != null && confirmedPassword != null
					&& !newPassword.equals(confirmedPassword)) {
				json.put("tag", "update-account");
				json.put("error", "password-not-match");
				json.put("message", "Password and Confirmed Password were not matched");
				return json.toString();
			} else {
				if (accountDAO.changePassword(id, oldPassword, newPassword)) {
					return this.getAccount(id);
				} else {
					json.put("tag", "update-account");
					json.put("error", "change-password-failed");
					json.put("message", "Update operation was failed when saving Account's Password into the system");
					return json.toString();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json.toString();
	}

	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public String login(@FormParam("username") String username, @FormParam("password") String password) {
		JSONObject json = new JSONObject();
		try {
			if ("".equals(username) || "".equals(password)) {
				json.put("tag", "login");
				json.put("error", "param-empty");
				json.put("message", "Please check login input!");
			} else {
				accountDAO = new AccountDAO();
				int id = accountDAO.login(username, password);
				System.out.println("IDlogin  = " + id);
				if (id > 0) {
					Account acc = new Account();
					acc = accountDAO.getAccount(id);
					acc.setAuthToken(accountDAO.getToken(id));
					System.out.println("TOKEN + " + acc.getAuthToken());
					return new ObjectMapper().writeValueAsString(acc);
				} else {
					json.put("tag", "login");
					json.put("error", "login-failed");
					json.put("message", "Please check login input!");
					return json.toString();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "NULL";
	}

}
