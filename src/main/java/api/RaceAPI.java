package api;

import java.io.IOException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import models.Race;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import dao.RaceDAO;

@Path("/race")
public class RaceAPI {
	RaceDAO raceDAO;
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAll() {
		raceDAO = new RaceDAO();
		ArrayList<Race> listRaces = raceDAO.getAll();
		try {
			if (listRaces.isEmpty()) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("tag", "view-all-races");
				jsonObj.put("error", "empty");
				jsonObj.put("message", "There is not any race was found in the system");
				return jsonObj.toString();
			} else {
				ObjectMapper om = new ObjectMapper();
				String json = om.writeValueAsString(listRaces);
				return json;
			}
		} catch (IOException e) {
		} catch (JSONException e) {
		}
		return null;
	}

	@GET
	@Path("/view")
	@Produces(MediaType.APPLICATION_JSON)
	public String get(@QueryParam("id") int id) {
		raceDAO = new RaceDAO();
		Race race = raceDAO.get(id);
		try {
			if (race != null) {
				return new ObjectMapper().writeValueAsString(race);
			} else {
				JSONObject json = new JSONObject();
				json.put("tag", "view-block");
				json.put("error", "not-found");
				json.put("message", "The Block which has ID=" + id + " is not found in the system");
				return json.toString();
			}
		} catch (Exception e) {
		}
		return null;
	}


	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public String create(@FormParam("name") String name,
			@FormParam("description") String description,
			@FormParam("total_growing_days") int totalGrowingDays) {
		raceDAO = new RaceDAO();
		Race race = new Race(0, name, description, totalGrowingDays);
		try {
			int newRaceId = raceDAO.create(race);
			if (newRaceId > 0) {
				return this.get(newRaceId);
			} else {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("tag", "create-race");
				jsonObj.put("error", "creation-failed");
				jsonObj.put("message", "Creation operation was failed when adding new Race into the system");
				return jsonObj.toString();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateAccount(@FormParam("id") int id,
			@FormParam("name") String name,
			@FormParam("description") String description,
			@FormParam("total_growing_days") int totalGrowingDays) {
		raceDAO = new RaceDAO();
		Race race = new Race(id, name, description, totalGrowingDays);
		try {
			if (raceDAO.update(race)) {
				return this.get(id);
			} else {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("tag", "update-block");
				jsonObj.put("error", "update-failed");
				jsonObj.put("message", "Update operation was failed when saving Block into the system");
				return jsonObj.toString();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateAccount(@QueryParam("id") int id) {
		raceDAO = new RaceDAO();
		try {
			JSONObject jsonObj = new JSONObject();
			if (raceDAO.delete(id)) {
				jsonObj.put("tag", "delete-race");
				jsonObj.put("success", "delete-success");
				jsonObj.put("message", "Race was deleted!");
			} else {
				jsonObj.put("tag", "delete-race");
				jsonObj.put("error", "delete-failed");
				jsonObj.put("message", "Delete operation was failed when remove Race from the system");
			}
			return jsonObj.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
}
