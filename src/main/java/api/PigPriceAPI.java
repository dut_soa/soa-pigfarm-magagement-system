package api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/pig-price")
public class PigPriceAPI {
	@GET
	@Path("/show")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCurrentPigPrice() {
		// FIXME
		return new String(
				"[{'timestamp': 1477701705000, 'race': {'id': 1, 'name': 'heo tai xanh', 'description': 'heo có tai màu xanh', 'totalGrowingDays': 100}, 'value': 50},"
						+ "{'timestamp': 1477702015000, 'race': {'id': 2, 'name': 'heo tai vàng', 'description': 'heo có tai màu vàng', 'totalGrowingDays': 120}, 'value': 100},"
						+ "{'timestamp': 1477702415300, 'race': {'id': 3, 'name': 'heo tai đỏ', 'description': 'heo có tai màu đỏ', 'totalGrowingDays': 150}, 'value': 150}]")
				.replace("\'", "\"");
	}

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFullPigPrice() {
		// FIXME
		return new String(
				"[{'timestamp': 1477601705000, 'race': {'id': 1, 'name': 'heo tai xanh', 'description': 'heo có tai màu xanh', 'totalGrowingDays': 100}, 'value': 60},"
						+ "{'timestamp': 1477602123000, 'race': {'id': 2, 'name': 'heo tai vàng', 'description': 'heo có tai màu vàng', 'totalGrowingDays': 120}, 'value': 110},"
						+ "{'timestamp': 1477603215030, 'race': {'id': 3, 'name': 'heo tai đỏ', 'description': 'heo có tai màu đỏ', 'totalGrowingDays': 150}, 'value': 140},"
						+ "{'timestamp': 1477712315123, 'race': {'id': 2, 'name': 'heo tai vàng', 'description': 'heo có tai màu vàng', 'totalGrowingDays': 120}, 'value': 100},"
						+ "{'timestamp': 1477723315001, 'race': {'id': 3, 'name': 'heo tai đỏ', 'description': 'heo có tai màu đỏ', 'totalGrowingDays': 150}, 'value': 150},"
						+ "{'timestamp': 1477734415300, 'race': {'id': 1, 'name': 'heo tai xanh', 'description': 'heo có tai màu xanh', 'totalGrowingDays': 100}, 'value': 50}]")
				.replace("\'", "\"");
	}

	@GET
	@Path("/view")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCurrentPigPriceById(@QueryParam("id") int id) {
		// FIXME
		return new String(
				"{'timestamp': 1477734415300, 'race': {'id': 1, 'name': 'heo tai xanh', 'description': 'heo có tai màu xanh', 'totalGrowingDays': 100}, 'value': 50}")
				.replace("\'", "\"");
	}
}
