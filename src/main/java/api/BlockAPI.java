package api;

import java.io.IOException;
import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import models.Block;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import dao.AccountDAO;
import dao.BlockDAO;

@Path("/block")
public class BlockAPI {
	BlockDAO blockDAO;
	AccountDAO accountDAO;

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAll() {
		blockDAO = new BlockDAO();
		ArrayList<Block> list = blockDAO.getAll();
		JSONObject jsonObj = new JSONObject();
		try {
			if (list.isEmpty()) {
				jsonObj.put("tag", "view-all-blocks");
				jsonObj.put("error", "empty");
				jsonObj.put("message", "There is not any block was found in the system");
				return jsonObj.toString();
			} else {
				ObjectMapper om = new ObjectMapper();
				String json = om.writeValueAsString(list);
				return json;
			}
		} catch (IOException e) {
		} catch (JSONException e) {
		}
		return jsonObj.toString();
	}

	@GET
	@Path("/view")
	@Produces(MediaType.APPLICATION_JSON)
	public String get(@QueryParam("id") int id) {
		blockDAO = new BlockDAO();
		Block block = blockDAO.get(id);
		try {
			if (block != null) {
				return new ObjectMapper().writeValueAsString(block);
			} else {
				JSONObject json = new JSONObject();
				json.put("tag", "view-block");
				json.put("error", "not-found");
				json.put("message", "The Block which has ID=" + id + " is not found in the system");
				return json.toString();
			}
		} catch (Exception e) {
		}
		return null;
	}


	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public String create(@FormParam("name") String name, @FormParam("managerId") int managerId) {
		JSONObject json = new JSONObject();
		blockDAO = new BlockDAO();
		Block block = new Block(0, name, new AccountDAO().getAccount(managerId));
		try {
			int newBlockId = blockDAO.create(block);
			if (newBlockId > 0) {
				return this.get(newBlockId);
			} else {
				json.put("tag", "create-block");
				json.put("error", "creation-failed");
				json.put("message", "Creation operation was failed when adding new Block into the system");
				return json.toString();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json.toString();
	}

	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateAccount(@FormParam("id") int id, @FormParam("name") String name,
			@FormParam("managerId") int managerId) {
		JSONObject json = new JSONObject();
		blockDAO = new BlockDAO();
		Block block = new Block(id, name, new AccountDAO().getAccount(managerId));
		try {
			if (blockDAO.update(block)) {
				return this.get(id);
			} else {
				json.put("tag", "update-block");
				json.put("error", "update-failed");
				json.put("message", "Update operation was failed when saving Block into the system");
				return json.toString();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json.toString();
	}

	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateAccount(@QueryParam("id") int id) {
		JSONObject json = new JSONObject();
		blockDAO = new BlockDAO();
		try {
			if (blockDAO.delete(id)) {
				json.put("tag", "delete-block");
				json.put("success", "delete-success");
				json.put("message", "Block was deleted!");
				return json.toString();
			} else {
				json.put("tag", "delete-block");
				json.put("error", "delete-failed");
				json.put("message", "Delete operation was failed when remove Block from the system");
				return json.toString();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json.toString();
	}

}