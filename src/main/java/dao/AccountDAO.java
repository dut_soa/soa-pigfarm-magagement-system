/**
 * 
 */
package dao;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import factories.AccountImpl;
import factories.Role;
import models.Account;

/**
 * @author ngbaanh
 *
 */
public class AccountDAO extends Database implements AccountImpl {
	/**
	 * 
	 */
	public AccountDAO() {
		this.role = Role.VISITOR;
	}
	
	public AccountDAO(Role role) {
		this.role = role;
	}
	
	/**
	 *  Methods for CRUD 
	 */
	
	/**
	 * Insert a new account to the system
	 * @param acc
	 * @return new created account ID
	 */
	@Override
	public int createAccount(Account acc) {
		int returnId = 0;
		if (acc.validate()) {
			try {
				String sql = "INSERT INTO account (username, password_hash, full_name, phone_number, email, address, role) "
						+ " VALUES (?, ?, ?, ?, ?, ?, ?)";
				Connection cnn = this.getConnection(role);
				PreparedStatement ps = cnn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, acc.getUsername());
				ps.setInt(2, acc.getPassword().hashCode());
				ps.setString(3, acc.getFullName());
				ps.setString(4, acc.getPhoneNumber());
				ps.setString(5, acc.getEmail());
				ps.setString(6, acc.getAddress());
				ps.setString(7, acc.getRole());
				System.out.println(ps);
				returnId = ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					returnId = rs.getInt(1);
				}
				ps.close();
				cnn.close();
				return returnId;
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			}
		} else {
			return 0;
		}
	}
	
	/**
	 * Get an existing account from the system
	 * @param id
	 * @return Account if existed, otherwise NULL
	 */
	@Override
	public Account getAccount(int id) {
		if (id <= 0) {
			return null;
		}		
		try {
			Account account = null;
			String sql = "SELECT * FROM account WHERE id = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setInt(1, id);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				account = new Account();
				account.setId(id);
				account.setUsername(rs.getString("username"));
				//account.setPassword("LOL"); //NO PASSWORD 
				account.setPasswordHash(Integer.parseInt(rs.getString("password_hash")));
				account.setFullName(rs.getString("full_name"));
				account.setEmail(rs.getString("email"));
				account.setAddress(rs.getString("address"));
				account.setPhoneNumber(rs.getString("phone_number"));
				account.setRole(rs.getString("role"));
				account.setDeleteAt(rs.getTimestamp("deleted_at"));
			}
			ps.close();
			cnn.close();
			return account;
		} catch (SQLException e) {
			return null;
		}
	}
	
	/**
	 * Soft remove an account from the system
	 * @param id
	 * @return TRUE on success, FALSE on failure
	 */
	@Override
	public boolean deleteAccount(int id) {
		try {
			String sql = "UPDATE account SET deleted_at = ? WHERE id = ? AND deleted_at IS NULL";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setTimestamp(1, new Timestamp(new Date().getTime()));
			ps.setInt(2, id);
			System.out.println(ps);
			boolean result = ps.executeUpdate() > 0;
			ps.close();
			cnn.close();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Update data of an existing account
	 * @param acc
	 * @return TRUE on success, FALSE on failure
	 */
	@Override
	public boolean updateAccount(Account acc) {
		if (acc.validate()) {
			System.out.println("Account DAO update");
			try {
				String sql = "UPDATE account SET full_name = ?, phone_number = ?, email = ?, address = ?, role = ? "
						+ " WHERE id = ? and password_hash = ?";
				Connection cnn = this.getConnection(role);
				PreparedStatement ps = cnn.prepareStatement(sql);
				ps.setString(1, acc.getFullName());
				ps.setString(2, acc.getPhoneNumber());
				ps.setString(3, acc.getEmail());
				ps.setString(4, acc.getAddress());
				ps.setString(5, acc.getRole());
				ps.setInt(6, acc.getId());
				ps.setInt(7, acc.getPassword().hashCode());
				System.out.println(ps);
				boolean result = ps.executeUpdate() > 0;
				ps.close();
				cnn.close();
				return result;
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
	}
	
	/* ==================================================== */
	
	/**
	 * Check if the Account username is not violent with the other
	 * @param acc
	 * @return
	 */
	@Override
	public boolean isUsernameUnique(Account acc) {
		try {
			String sql = "SELECT COUNT(username) FROM account WHERE username = ? AND deleted_at IS NULL";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setString(1, acc.getUsername());
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			int count = 0;
			if (rs.next()) {
				count = rs.getInt(1);
				System.out.println("Username Count = " + count);
			};
			ps.close();
			cnn.close();
			return (count == 1); // only ONE
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * Get all accounts, except soft deleted accounts
	 * @return List of Account
	 */
	@Override
	public ArrayList<Account> getAllAccounts() {
		try {
			ArrayList<Account> list = new ArrayList<Account>();
			String sql = "SELECT id FROM account WHERE deleted_at IS NULL";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Account acc = this.getAccount(rs.getInt("id"));
				//acc.setPassword("Hidden-PASS");
				acc.setPasswordHash(0);
				list.add(acc);
			}
			ps.close();
			cnn.close();
			return list;
		} catch (SQLException e) {
			return null;
		}
	}
	/**
	 * Change password of an existing account after doing an validation [!]
	 * Remember to validate password first before perform this method!
	 * @param accountId
	 * @param newPassword
	 * @return TRUE on success, FALSE on failure
	 */
	@Override
	public boolean changePassword(int accountId, String oldPassword, String newPassword) {
		try {
			String sql = "UPDATE account SET password_hash = ? WHERE id = ? AND password_hash = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setInt(1, newPassword.hashCode());
			ps.setInt(2, accountId);
			ps.setInt(3, oldPassword.hashCode());
			System.out.println(ps);
			boolean result = ps.executeUpdate() > 0;
			ps.close();
			cnn.close();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Hard remove an account from the system. 
	 * @param id.
	 * @return TRUE on success, FALSE on failure
	 */
	@Override
	public boolean deleteAccountPermanently(int id) {
		try {
			String sql = "DELETE FROM account WHERE id = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setInt(1, id);
			System.out.println(ps);
			boolean result = ps.executeUpdate() > 0;
			ps.close();
			cnn.close();
			return result;
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * Check login and update authentication token
	 * @param username
	 * @param password
	 * @return account id if success
	 */
	@Override
	public int login(String username, String password) {
	    try {
			String authentication = "SELECT id FROM account where username = ? and password_hash = ?";
			String updateToken = "UPDATE account SET auth_token = ? where id = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(authentication);
			ps.setString(1, username);
			ps.setInt(2, password.hashCode());
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int id = rs.getInt(1);
				Role role = Database.getAccessRole(id); // Đổi Role
				System.out.println("Access ROLE = " + role);
				Connection cnn2 = this.getConnection(role);
				PreparedStatement ps2 = cnn2.prepareStatement(updateToken);
				SecureRandom random = new SecureRandom();
			    byte bytes[] = new byte[20];
			    random.nextBytes(bytes);
			    String token = bytes.toString();
			    ps2.setString(1, token);
			    ps2.setInt(2, id);
			    System.out.println(ps2);
			    ps2.execute();
			    ps2.close();
			    ps.close();
			    cnn.close();
				return id;
			} else {
				ps.close();
			    cnn.close();
				return 0;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	@Override
	public String getToken(int id) {
		try {
			String sql = "SELECT auth_token FROM account WHERE id = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setInt(1, id);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			String result = null;
			if (rs.next()) {
				result = rs.getString(1);
			}
			ps.close();
			cnn.close();
			return result;
		} catch (SQLException e) {
			return null;
		}
	}
	
	@Override
	public Role getRoleFromToken(String token) {
		try {
			String sql = "SELECT id FROM account WHERE auth_token = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setString(1, token);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			Role result = Role.VISITOR;
			if (rs.next()) {
				String role = this.getAccount(rs.getInt("id")).getRole();
				switch (role) {
				case "admin":
					return Role.ADMIN;
				case "manager":
					return Role.MANAGER;
				case "worker":
					return Role.WORKER;
				default:
					return Role.VISITOR;
				}
			}
			ps.close();
			cnn.close();
			return result;
		} catch (SQLException e) {
			return Role.VISITOR;
		}
	}
	
	@Override
	public Account getAccountByToken(String token) {
		try {
			String sql = "SELECT id FROM account WHERE auth_token = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setString(1, token);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			Account acc = null;
			if (rs.next()) {
				acc = this.getAccount(rs.getInt("id"));
			}
			ps.close();
			cnn.close();
			return acc;
		} catch (SQLException e) {
			return null;
		}
	}
	

}
