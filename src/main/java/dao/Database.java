/**
 * 
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;

import factories.Role;
import models.Account;

/**
 * @author ngbaanh
 *
 */
public abstract class Database {
	protected Role role;

	/**
	 * Connect to Database based on Role
	 * @param role
	 * @return
	 */
	protected Connection getConnection(Role role) {
		switch (role) {
		case ADMIN: {
			return this.getConnection("soa_admin", "soa_admin");
		}
		case MANAGER: {
			return this.getConnection("soa_manager", "soa_manager");
		}
		case WORKER: {
			return this.getConnection("soa_worker", "soa_worker");
		}
		default: {
			return this.getConnection("soa_visitor", "soa_visitor");
		}
		}
	}
	
	/**
	 * Connect to Database 
	 * @param user
	 * @param pass
	 * @return
	 */
	protected Connection getConnection(String user, String pass) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String address = "jdbc:mysql://localhost:3306/soa";
			String options = "?useUnicode=true&characterEncoding=utf-8";
			System.out.println("Connected as " + user);
			return DriverManager.getConnection(address + options, user, pass);
		} catch (Exception e) {
			System.err.println("[Database] Lỗi: " + e);
			return null;
		}
	}

	/**
	 * Get Role from account id
	 * @param id
	 * @return
	 */
	public static Role getAccessRole(int id) {
		Account acc = new AccountDAO().getAccount(id);
		if (acc == null) {
			return Role.VISITOR;
		} else {
			switch (acc.getRole()) {
			case "admin":
				return Role.ADMIN;
			case "manager":
				return Role.MANAGER;
			case "worker":
				return Role.WORKER;
			default:
				return Role.VISITOR;
			}
		}
	}
	
	/**
	 * Get Role from Token
	 * @param token
	 * @return
	 */
	public static Role getRole(String token) {
		return new AccountDAO().getRoleFromToken(token);
	}

}