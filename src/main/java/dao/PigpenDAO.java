/**
 * 
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import factories.PigpenImpl;
import factories.Role;
import models.Block;
import models.Pigpen;

/**
 * @author ngbaanh
 *
 */
public class PigpenDAO extends Database implements PigpenImpl {
	Role role;
	
	public PigpenDAO() {
		this.role = Role.VISITOR;
	}
	
	public PigpenDAO(Role role) {
		this.role = role;
	}
	
	@Override
	public int create(int id, int blockId) {
		int returnId = 0;
		if (blockId > 0) {
			try {
				String sql = "INSERT INTO pigpen (block_id) VALUES (?)";
				Connection cnn = this.getConnection(role);
				PreparedStatement ps = cnn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setInt(1, blockId);
				System.out.println(ps);
				returnId = ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					returnId = rs.getInt(1);
				}
				ps.close();
				cnn.close();
				return returnId;
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			}
		} else {
			return 0;
		}
	}
	
	@Override
	public Pigpen get(int id) {
		if (id <= 0) {
			return null;
		}		
		try {
			Pigpen pigpen = null;
			String sql = "SELECT * FROM pigpen WHERE id = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setInt(1, id);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				pigpen = new Pigpen();
				pigpen.setId(id);
				pigpen.setBlock(new BlockDAO().get(rs.getInt("block_id")));
				pigpen.setPig(new PigDAO().get(rs.getInt("pig_id")));
			}
			ps.close();
			cnn.close();
			return pigpen;
		} catch (SQLException e) {
			return null;
		}
	}
 
	@Override
	public boolean update(Block block) { //TODO
		if (block.validate()) {
			try {
				String sql = "UPDATE block SET name = ?, manager_id = ? WHERE id = ?";
				Connection cnn = this.getConnection(role);
				PreparedStatement ps = cnn.prepareStatement(sql);
				ps.setString(1, block.getName());
				ps.setInt(2, block.getManager().getId());
				ps.setInt(3, block.getId());
				System.out.println(ps);
				boolean result = ps.executeUpdate() > 0;
				ps.close();
				cnn.close();
				return result;
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<Pigpen> getAll() {
		try {
			ArrayList<Pigpen> list = new ArrayList<Pigpen>();
			String sql = "SELECT id FROM pigpen";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(this.get(rs.getInt("id")));
			}
			ps.close();
			cnn.close();
			return list;
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public ArrayList<Pigpen> getAllInBlock(int blockId) {
		try {
			ArrayList<Pigpen> list = new ArrayList<Pigpen>();
			String sql = "SELECT id FROM pigpen where block_id = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setInt(1, blockId);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(this.get(rs.getInt("id")));
			}
			ps.close();
			cnn.close();
			return list;
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public boolean addPig(int pigId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removePig(int pigId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean movePig(int fromPigId, int toPigpenId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean swapPig(int pigpenId1, int pigpenId2) {
		// TODO Auto-generated method stub
		return false;
	}

}
