/**
 * 
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import factories.PigImpl;
import factories.Role;
import models.Health;
import models.Pig;

/**
 * @author ngbaanh
 *
 */
public class PigDAO extends Database implements PigImpl {
	Role role;
	public PigDAO() {
		//super();
		this.role = Role.VISITOR;
	}
	public PigDAO(Role role) {
		this.role = role;
	}

	@Override
	public int create(Pig pig) {
		return 0; //TODO
	}
	
	@Override
	public boolean update(Pig pig) {
		return false; //TODO
	}
	
	@Override
	public boolean delete(int id) {
		return false; //TODO
	}
	
	@Override
	public Pig get(int id) {
		if (id <= 0) {
			return null;
		}		
		try {
			Pig pig = null;
			String sql = "SELECT * FROM pig WHERE id = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setInt(1, id);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				pig = new Pig();
				pig.setId(id);
				pig.setRace(new RaceDAO().get(rs.getInt("race_id")));
				pig.setWeight(rs.getFloat("weight"));
				pig.setGender(rs.getBoolean("gender"));
				pig.setSoldAt(rs.getTimestamp("sold_at"));
				pig.setCreatedAt(rs.getTimestamp("created_at"));
				pig.setUpdatedAt(rs.getTimestamp("updated_at"));
				pig.setDeletedAt(rs.getTimestamp("deleted_at"));
			}
			ps.close();
			cnn.close();
			return pig;
		} catch (SQLException e) {
			return null;
		}
	}
	
	@Override
	public boolean sell(int id) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void updateHealth(Health health) {
		// TODO Auto-generated method stub
		
	}
}
