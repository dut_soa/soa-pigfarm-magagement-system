/**
 * 
 */
package dao;

import java.sql.Timestamp;
import java.util.ArrayList;

import factories.Role;
import factories.TradeImpl;
import models.Trade;

/**
 * @author ngbaanh
 *
 */
public class TradeDAO extends Database implements TradeImpl {

	/**
	 * 
	 */
	public TradeDAO() {
		this.role = Role.VISITOR;
	}
	
	public TradeDAO(Role role) {
		this.role = role;
	}

	/* (non-Javadoc)
	 * @see factories.TradeImpl#create(models.Trade)
	 */
	@Override
	public int create(Trade trade) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see factories.TradeImpl#update(models.Trade)
	 */
	@Override
	public boolean update(Trade trade) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see factories.TradeImpl#delete(int)
	 */
	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see factories.TradeImpl#get(int)
	 */
	@Override
	public Trade get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see factories.TradeImpl#getAll()
	 */
	@Override
	public ArrayList<Trade> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see factories.TradeImpl#getSegment(java.sql.Timestamp, java.sql.Timestamp)
	 */
	@Override
	public ArrayList<Trade> getSegment(Timestamp from, Timestamp to) {
		// TODO Auto-generated method stub
		return null;
	}

}
