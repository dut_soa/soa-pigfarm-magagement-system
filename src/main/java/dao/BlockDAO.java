/**
 * 
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import factories.BlockImpl;
import factories.Role;
import models.Block;

/**
 * @author ngbaanh
 *
 */
public class BlockDAO extends Database implements BlockImpl {
	
	public BlockDAO() {
		this.role = Role.VISITOR;
	}

	public BlockDAO(Role role) {
		this.role = role;
	}

	@Override
	public int create(Block block) {
		int returnId = 0;
		if (block.validate()) {
			try {
				String sql = "INSERT INTO block (name, manager_id) VALUES (?, ?)";
				Connection cnn = this.getConnection(this.role);
				PreparedStatement ps = cnn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, block.getName());
				ps.setInt(2, block.getManager().getId());
				System.out.println(ps);
				returnId = ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					returnId = rs.getInt(1);
				}
				ps.close();
				cnn.close();
				return returnId;
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			}
		} else {
			return 0;
		}
	}

	@Override
	public Block get(int id) {
		if (id <= 0) {
			return null;
		}
		try {
			Block block = null;
			String sql = "SELECT * FROM block WHERE id = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setInt(1, id);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				block = new Block();
				block.setId(id);
				block.setName(rs.getString("name"));
				block.setManager(new AccountDAO().getAccount(rs.getInt("manager_id")));
			}
			ps.close();
			cnn.close();
			return block;
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public boolean update(Block block) {
		if (block.validate()) {
			try {
				String sql = "UPDATE block SET name = ?, manager_id = ? WHERE id = ?";
				Connection cnn = this.getConnection(this.role);
				PreparedStatement ps = cnn.prepareStatement(sql);
				ps.setString(1, block.getName());
				ps.setInt(2, block.getManager().getId());
				ps.setInt(3, block.getId());
				System.out.println(ps);
				boolean result = ps.executeUpdate() > 0;
				ps.close();
				cnn.close();
				return result;
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<Block> getAll() {
		try {
			ArrayList<Block> list = new ArrayList<Block>();
			String sql = "SELECT id FROM block ORDER BY id ASC";
			Connection cnn = this.getConnection(this.role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				list.add(this.get(rs.getInt("id")));
			}
			ps.close();
			cnn.close();
			return list;
		} catch (SQLException e) {
			return null;
		}
	}

}
