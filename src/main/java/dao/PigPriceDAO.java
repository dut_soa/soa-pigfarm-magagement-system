/**
 * 
 */
package dao;

import java.sql.Timestamp;
import java.util.ArrayList;

import factories.PigPriceImpl;
import factories.Role;
import models.PigPrice;

/**
 * @author ngbaanh
 *
 */
public class PigPriceDAO extends Database implements PigPriceImpl {

	/**
	 * 
	 */
	public PigPriceDAO() {
		this.role = Role.VISITOR;
	}
	
	public PigPriceDAO(Role role) {
		this.role = role;
	}

	/* (non-Javadoc)
	 * @see factories.PigPriceImpl#create(models.PigPrice)
	 */
	@Override
	public Timestamp create(PigPrice pigPrice) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see factories.PigPriceImpl#update(models.PigPrice)
	 */
	@Override
	public boolean update(PigPrice pigPrice) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see factories.PigPriceImpl#delete(java.sql.Timestamp)
	 */
	@Override
	public boolean delete(Timestamp t) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see factories.PigPriceImpl#get(java.sql.Timestamp)
	 */
	@Override
	public PigPrice get(Timestamp t) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see factories.PigPriceImpl#getAll()
	 */
	@Override
	public ArrayList<PigPrice> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see factories.PigPriceImpl#getAllByRace(int)
	 */
	@Override
	public ArrayList<PigPrice> getAllByRace(int raceId) {
		// TODO Auto-generated method stub
		return null;
	}

}
