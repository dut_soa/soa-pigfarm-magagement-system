/**
 * 
 */
package dao;

import factories.HealthImpl;
import factories.Role;
import models.Health;

/**
 * @author ngbaanh
 *
 */
public class HealthDAO extends Database implements HealthImpl {

	/**
	 * 
	 */
	public HealthDAO() {
		this.role = Role.VISITOR;
	}
	public HealthDAO(Role role) {
		this.role = role;
	}

	/* (non-Javadoc)
	 * @see factories.HealthImpl#create(models.Health)
	 */
	@Override
	public int create(Health health) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see factories.HealthImpl#update(models.Health)
	 */
	@Override
	public boolean update(Health health) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see factories.HealthImpl#delete(int)
	 */
	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see factories.HealthImpl#get(int)
	 */
	@Override
	public Health get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
