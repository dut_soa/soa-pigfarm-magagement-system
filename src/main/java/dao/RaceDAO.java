/**
 * 
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import factories.RaceImpl;
import factories.Role;
import models.Race;

/**
 *
 *
 */
public class RaceDAO extends Database implements RaceImpl {
	/**
	 * 
	 */
	public RaceDAO() {
		this.role = Role.VISITOR;
	}
	
	public RaceDAO(Role role) {
		this.role = role;
	}
	
	@Override
	public int create(Race race) {
		if (race.isValid()) {
			try {
				int returnId = 0;
				String sql = "INSERT INTO race (name, description, total_growing_days) VALUES (?, ?, ?)";
				Connection cnn = this.getConnection(this.role);
				PreparedStatement ps = cnn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, race.getName());
				ps.setString(2, race.getDescription());
				ps.setInt(3, race.getTotalGrowingDays());
				System.out.println(ps);
				returnId = ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					returnId = rs.getInt(1);
				}
				ps.close();
				cnn.close();
				return returnId;
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			}
		} else {
			return 0;
		}
	}
	
	@Override
	public boolean update(Race race) {
		if (race.isValid()) {
			try {
				String sql = "UPDATE race SET name = ?, description = ?, total_growing_days = ? WHERE id = ?";
				Connection cnn = this.getConnection(this.role);
				PreparedStatement ps = cnn.prepareStatement(sql);
				ps.setString(1, race.getName());
				ps.setString(2, race.getDescription());
				ps.setInt(3, race.getTotalGrowingDays());
				ps.setInt(4, race.getId());
				System.out.println(ps);
				boolean result = ps.executeUpdate() > 0;
				ps.close();
				cnn.close();
				return result;
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
	}
	
	@Override
	public boolean delete(int id) {
		return false; //TODO
	}
	
	@Override
	public Race get(int id) {
		if (id <= 0) {
			return null;
		}		
		try {
			Race race = null;
			String sql = "SELECT * FROM race WHERE id = ?";
			Connection cnn = this.getConnection(role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setInt(1, id);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				race = new Race();
				race.setId(id);
				race.setName(rs.getString("name"));
				race.setDescription(rs.getString("description"));
				race.setTotalGrowingDays(rs.getInt("total_growing_days"));
			}
			ps.close();
			cnn.close();
			return race;
		} catch (SQLException e) {
			return null;
		}
	}
	
	@Override
	public ArrayList<Race> getAll() {
		try {
			ArrayList<Race> listRaces = new ArrayList<Race>();
			String sql = "SELECT id FROM race";
			Connection cnn = this.getConnection(this.role);
			PreparedStatement ps = cnn.prepareStatement(sql);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				listRaces.add(this.get(rs.getInt("id")));
			}
			ps.close();
			cnn.close();
			return listRaces;
		} catch (SQLException e) {
			return null;
		}
	}
}
