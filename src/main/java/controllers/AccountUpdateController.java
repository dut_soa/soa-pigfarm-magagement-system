package controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AccountUpdateController
 */
@WebServlet("/account/update")
public class AccountUpdateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccountUpdateController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		String viewPath = new String("/WEB-INF/views/account/update.jsp");
		try {
			// get and send required param for Client
			int id = Integer.parseInt(request.getParameter("id"));
			request.setAttribute("id", id);
			// render view
			request.getRequestDispatcher(viewPath).forward(request, response);
		} catch (NumberFormatException e) {
			response.getWriter().append("Error on parsing 'id' parameter!");
		}
	}

}
