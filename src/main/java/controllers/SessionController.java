package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import models.Account;

/**
 * Servlet implementation class SessionController
 */
@WebServlet("/session")
public class SessionController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SessionController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		System.out.print("Session Controller: ");
		String action = request.getParameter("action");
		if ("setToken".equals(action)) {
			String token = request.getParameter("token");
			if (!"".equals(token)) {
				HttpSession sess = request.getSession();
				sess.setAttribute("token", token);
				try {
					Account acc = new AccountDAO().getAccountByToken(token);
					sess.setAttribute("fullName", acc.getFullName());
					sess.setAttribute("accountId", acc.getId());
				} catch (NullPointerException e) {}				
				response.getWriter().append("{ \"success\" : \"Token was set sucessfully.\"}");
			} else {
				response.getWriter().append("{ \"error\" : \"Token was null or blank.\"}");
			}
			
		} else if ("destroy".equals(action)) {
			request.getSession().invalidate();
			response.getWriter().append("{\"success\" : \"Token was destroyed sucessfully.\"}");
		}
		
	}

}
