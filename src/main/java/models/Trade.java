/**
 * 
 */
package models;

import java.sql.Timestamp;

/**
 * @author ngbaanh
 *
 */
public class Trade {
	Timestamp timestamp;
	Pig pig;
	float priceValue; // price per unit (kg)
	Account manager;
	float totalPrice; // sale price base on weight
	float totalCost; // cost spent during the growth
	
	/**
	 * 
	 */
	public Trade() {
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public Pig getPig() {
		return pig;
	}

	public void setPig(Pig pig) {
		this.pig = pig;
	}

	public float getPriceValue() {
		return priceValue;
	}

	public void setPriceValue(float priceValue) {
		this.priceValue = priceValue;
	}

	public Account getManager() {
		return manager;
	}

	public void setManager(Account manager) {
		this.manager = manager;
	}

	public float getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}

	public float getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(float totalCost) {
		this.totalCost = totalCost;
	}
	
	

}
