/**
 * 
 */
package models;

/**
 * @author ngbaanh
 *
 */
public class Block {
	int id;
	String name;
	Account manager;
	
	public Block() {
	}
	
	
	
	public Block(int id, String name, Account manager) {
		super();
		this.id = id;
		this.name = name;
		this.manager = manager;
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Account getManager() {
		return manager;
	}
	public void setManager(Account manager) {
		this.manager = manager;
	}



	public boolean validate() {
		if ("".equals(this.name.trim())	|| 50 < this.name.trim().length()) {
			System.out.println("Block not Validated!");
			return false;
		} else {
			System.out.println("Block Validated!");
			return true;
		}
	}
	
	

}
