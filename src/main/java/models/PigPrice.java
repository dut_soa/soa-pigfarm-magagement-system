/**
 * 
 */
package models;

import java.sql.Timestamp;

/**
 * @author ngbaanh
 *
 */
public class PigPrice {
	Timestamp timestamp;
	Race race;
	float value;
	/**
	 * 
	 */
	public PigPrice() {
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	public Race getRace() {
		return race;
	}
	public void setRace(Race race) {
		this.race = race;
	}
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}
	
	

}
