/**
 * 
 */
package models;

import java.sql.Timestamp;

/**
 * @author ngbaanh
 *
 */
public class Pig {
	int id;
	Race race;
	float weight;
	boolean gender;
	Timestamp createdAt, updatedAt, deletedAt, soldAt;
	/**
	 * 
	 */
	public Pig() {
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Race getRace() {
		return race;
	}
	public void setRace(Race race) {
		this.race = race;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public boolean isGender() {
		return gender;
	}
	public void setGender(boolean gender) {
		this.gender = gender;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Timestamp getDeletedAt() {
		return deletedAt;
	}
	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}
	public Timestamp getSoldAt() {
		return soldAt;
	}
	public void setSoldAt(Timestamp soldAt) {
		this.soldAt = soldAt;
	}
	
	

}
