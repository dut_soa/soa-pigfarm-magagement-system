/**
 * 
 */
package models;

import java.sql.Timestamp;

/**
 * @author ngbaanh
 *
 */
public class HealthJournal {
	Timestamp timestamp;
	Pig pig;
	Health health;
	String note;
	Account worker;
	/**
	 * 
	 */
	public HealthJournal() {
		// TODO Auto-generated constructor stub
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	public Pig getPig() {
		return pig;
	}
	public void setPig(Pig pig) {
		this.pig = pig;
	}
	public Health getHealth() {
		return health;
	}
	public void setHealth(Health health) {
		this.health = health;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Account getWorker() {
		return worker;
	}
	public void setWorker(Account worker) {
		this.worker = worker;
	}
	
	

}
