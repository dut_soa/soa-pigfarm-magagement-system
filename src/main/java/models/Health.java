/**
 * 
 */
package models;

/**
 * @author ngbaanh
 *
 */
public class Health {
	int id;
	String rate, description;
	
	/**
	 * 
	 */
	public Health() {	
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
