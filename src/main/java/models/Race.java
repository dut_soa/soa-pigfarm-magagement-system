/**
 * 
 */
package models;

/**
 * @author ngbaanh
 *
 */
public class Race {
	int id;
	String name, description;
	int totalGrowingDays;
	
	public Race() {
	}
	
	public Race(int id, String name, String description, int totalGrowingDays) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.totalGrowingDays = totalGrowingDays;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getTotalGrowingDays() {
		return totalGrowingDays;
	}

	public void setTotalGrowingDays(int totalGrowingDays) {
		this.totalGrowingDays = totalGrowingDays;
	}

	/**
	 * Initial validation of input Data of an Race
	 * 
	 * @return true if the input is acceptable, otherwise false.
	 */
	public boolean isValid() {
		if ("".equals(this.name.trim())
				|| "".equals(this.description.trim())
				|| "".equals(this.totalGrowingDays)
				|| 50 < this.name.trim().length()
				|| 500 < this.description.trim().length()
				|| 0 > this.totalGrowingDays
				|| 500 < this.totalGrowingDays) {
			System.out.println("Race not Validated!");
			return false;
		} else {
			System.out.println("Race Validated!");
			return true;
		}
	}

}
