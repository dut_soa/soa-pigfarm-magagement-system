/**
 * 
 */
package models;

/**
 * @author ngbaanh
 *
 */
public class Pigpen {
	int id;
	Block block;
	Pig pig;

	/**
	 * 
	 */
	public Pigpen() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}

	public Pig getPig() {
		return pig;
	}

	public void setPig(Pig pig) {
		this.pig = pig;
	}

	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}
	
	

}
