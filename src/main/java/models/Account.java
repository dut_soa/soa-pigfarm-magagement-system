/**
 * 
 */
package models;

import java.sql.Timestamp;

/**
 * @author ngbaanh
 *
 */
public class Account {
	int id, passwordHash;
	String username, password, authToken;
	String fullName, phoneNumber, email, address, role;
	Timestamp deleteAt;

	/**
	 * 
	 */
	public Account() {
	}

	public Account(int id, String username, String password, int passwordHash, String fullName, String phoneNumber,
			String email, String address, String role, Timestamp deleteAt) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.passwordHash = passwordHash;
		this.fullName = fullName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.address = address;
		this.role = role;
		this.deleteAt = deleteAt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(int passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Timestamp getDeleteAt() {
		return deleteAt;
	}

	public void setDeleteAt(Timestamp deleteAt) {
		this.deleteAt = deleteAt;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	/**
	 * Initial validation of input Data of an Account
	 * 
	 * @return true if the input is acceptable, otherwise false.
	 */
	public boolean validate() {
		if ("".equals(this.username.trim()) || "".equals(this.password.trim()) || "".equals(this.fullName.trim())
				|| "".equals(this.address.trim()) || "".equals(this.phoneNumber.trim()) || "".equals(this.role.trim())
				|| 50 < this.username.trim().length() || 50 < this.password.trim().length()
				|| 100 < this.fullName.trim().length() || 100 < this.email.trim().length()
				|| 15 < this.phoneNumber.trim().length() || 100 < this.address.trim().length()
				|| 50 < this.role.trim().length()) {
			System.out.println("Account not Validated!");
			return false;
		} else {
			System.out.println("Account Validated!");
			return true;
		}
	}

}
