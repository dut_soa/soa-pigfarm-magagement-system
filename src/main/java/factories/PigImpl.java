/**
 * 
 */
package factories;

import models.Health;
import models.Pig;

/**
 * @author ngbaanh
 *
 */
public interface PigImpl {
	public int create(Pig pig);
	public boolean update(Pig pig);
	public boolean delete(int id);
	public Pig get(int id);
	
	public boolean sell(int id);
	public void updateHealth(Health health);	
	
}
