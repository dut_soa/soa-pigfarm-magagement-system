/**
 * 
 */
package factories;

import java.sql.Timestamp;
import java.util.ArrayList;

import models.Trade;

/**
 * @author ngbaanh
 *
 */
public interface TradeImpl {
	public int create(Trade trade);
	public boolean update(Trade trade);
	public boolean delete(int id);
	public Trade get(int id);
	public ArrayList<Trade> getAll();
	
	public ArrayList<Trade> getSegment(Timestamp from, Timestamp to);
}
