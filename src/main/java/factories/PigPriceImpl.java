/**
 * 
 */
package factories;

import java.sql.Timestamp;
import java.util.ArrayList;

import models.PigPrice;

/**
 * @author ngbaanh
 *
 */
public interface PigPriceImpl {
	public Timestamp create(PigPrice pigPrice);
	public boolean update(PigPrice pigPrice); 
	public boolean delete(Timestamp t);
	public PigPrice get(Timestamp t);
	public ArrayList<PigPrice> getAll();
	public ArrayList<PigPrice> getAllByRace(int raceId);
}
