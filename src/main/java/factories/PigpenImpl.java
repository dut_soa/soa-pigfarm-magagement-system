package factories;

import java.util.ArrayList;

import models.Block;
import models.Pigpen;

public interface PigpenImpl {
	public int create(int id, int blockId);
	public Pigpen get(int id);
	public boolean update(Block block);
	public boolean delete(int id);
	public ArrayList<Pigpen> getAll();
	public ArrayList<Pigpen> getAllInBlock(int blockId);
	
	public boolean addPig(int pigId);
	public boolean removePig(int pigId);
	public boolean movePig(int fromPigId, int toPigpenId);
	
	public boolean swapPig(int pigpenId1, int pigpenId2); 
}
