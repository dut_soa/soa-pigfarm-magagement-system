/**
 * 
 */
package factories;

/**
 * @author ngbaanh
 *
 */
public enum Role {
	VISITOR, WORKER, MANAGER, ADMIN
}
