/**
 * 
 */
package factories;

import java.util.ArrayList;

import models.Account;

/**
 * @author ngbaanh
 *
 */
public interface AccountImpl {
	public int createAccount(Account acc);
	public Account getAccount(int id);
	public boolean deleteAccount(int id);
	public boolean updateAccount(Account acc);
	public boolean isUsernameUnique(Account acc);
	public ArrayList<Account> getAllAccounts();
	public boolean changePassword(int accountId, String oldPassword, String newPassword);
	public boolean deleteAccountPermanently(int id);
	public int login(String username, String password);
	public String getToken(int id);
	public Role getRoleFromToken(String token);
	public Account getAccountByToken(String token);
}
