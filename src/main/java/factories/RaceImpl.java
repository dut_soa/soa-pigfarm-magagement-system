/**
 * 
 */
package factories;

import java.util.ArrayList;

import models.Race;

/**
 * @author ngbaanh
 *
 */
public interface RaceImpl {
	public int create(Race race);
	public boolean update(Race race);
	public boolean delete(int id);
	public Race get(int id);
	public ArrayList<Race> getAll();
	
}
