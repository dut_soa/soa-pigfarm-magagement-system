/**
 * 
 */
package factories;

import models.Health;

/**
 * @author ngbaanh
 *
 */
public interface HealthImpl {
	public int create(Health health);
	public boolean update(Health health);
	public boolean delete(int id);
	public Health get(int id);
}
