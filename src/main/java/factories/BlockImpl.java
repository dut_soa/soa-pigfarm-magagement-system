/**
 * 
 */
package factories;

import java.util.ArrayList;

import models.Block;

/**
 * @author ngbaanh
 *
 */
public interface BlockImpl {
	public int create(Block block);
	public Block get(int id);
	public boolean update(Block block);
	public boolean delete(int id);
	public ArrayList<Block> getAll();
	
}
