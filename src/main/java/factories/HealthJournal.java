/**
 * 
 */
package factories;

import java.sql.Timestamp;

/**
 * @author ngbaanh
 *
 */
public interface HealthJournal {
	public Timestamp create(HealthJournal hJournal);
	public boolean update(HealthJournal hJournal);
	public boolean delete(Timestamp t);
	public HealthJournal get(Timestamp t);
}
